package ro.galanton.learn.microservices.restaurant.service;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ro.galanton.learn.microservices.restaurant.model.Table;
import ro.galanton.learn.microservices.restaurant.repository.TableRepository;

@Component
@Transactional
public class TableService {

	private final TableRepository repository;

	public TableService(TableRepository repository) {
		this.repository = repository;
	}

	public Table create(int number, int seats) {
		Table table = new Table();
		table.setNumber(number);
		table.setSeats(seats);
		
		return repository.save(table);
	}
	
	public Table find(int number) {
		return repository.findOne(number);
	}
	
	@Transactional(readOnly = true)
	public Iterable<Table> list() {
		return repository.findAll();
	}
	
	public Table update(int number, int seats) {
		Table table = repository.findOne(number);
		table.setSeats(seats);
		return repository.save(table);
	}
	
	public void delete(int number) {
		repository.delete(number);
	}
}
