package ro.galanton.learn.microservices.restaurant.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ro.galanton.learn.microservices.restaurant.model.Table;
import ro.galanton.learn.microservices.restaurant.service.TableService;
import ro.galanton.learn.microservices.restaurant.service.UiService;

@Controller
public class TablesController {

	private final UiService uiService;
	private final TableService tableService;

	public TablesController(UiService uiService, TableService tableService) {
		this.uiService = uiService;
		this.tableService = tableService;
	}
	
	@GetMapping("/tables")
	public String index(Model model) {
		addCommonAttributes(model);
		return "tables";
	}
	
	@Secured("ROLE_MANAGER")
	@PostMapping(value = "/tables", params = "create")
	public String create(@RequestParam("number") int number, @RequestParam("seats") int seats, Model model) {
		// TODO: handle exception when table number already exists
		Table table = tableService.create(number, seats);
		
		addCommonAttributes(model);
		model.addAttribute("message", "Table " + table.getNumber() + " created!");
		
		return "tables";
	}
	
	@Secured("ROLE_MANAGER")
	@PostMapping(value = "/tables", params = "delete")
	public String delete(@RequestParam("number") int number, Model model) {
		tableService.delete(number);
		
		addCommonAttributes(model);
		model.addAttribute("message", "Table " + number + " deleted");
		
		return "tables";
	}
	
	private void addCommonAttributes(Model model) {
		model.addAttribute("menu", uiService.getMenu());
		model.addAttribute("path", "/tables");
		model.addAttribute("tables", tableService.list());
	}
}
