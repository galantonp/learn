package ro.galanton.learn.microservices.restaurant.model.ui;

public class MenuItem {

	private final String displayName;
	private final String path;
	
	public MenuItem(String displayName, String path) {
		super();
		this.displayName = displayName;
		this.path = path;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getPath() {
		return path;
	}

}
