package ro.galanton.learn.microservices.restaurant.repository;

import org.springframework.data.repository.CrudRepository;

import ro.galanton.learn.microservices.restaurant.model.Order;

public interface OrderRepository extends CrudRepository<Order, Integer> {

}
