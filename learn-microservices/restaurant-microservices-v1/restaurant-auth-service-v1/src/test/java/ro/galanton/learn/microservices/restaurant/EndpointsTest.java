package ro.galanton.learn.microservices.restaurant;

import static io.restassured.config.LogConfig.logConfig;
import static io.restassured.config.RedirectConfig.redirectConfig;
import static io.restassured.config.RestAssuredConfig.config;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.RestAssured;
import io.restassured.filter.log.LogDetail;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AuthService.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EndpointsTest {

	@Value("${local.server.port}")
	private int port;

	@Before
	public final void initRestAssured() {
		RestAssured.port = port;
		RestAssured.config = config()
				.redirect(redirectConfig().followRedirects(false))
				.logConfig(logConfig().enableLoggingOfRequestAndResponseIfValidationFails(LogDetail.ALL));
	}
	
	@Test
	public void token_endpoint_is_working() {
		given()
			.auth().basic("restaurant-monolith", "123")
			.param("grant_type", "password")
			.param("username", "waiter")
			.param("password", "123")
			.param("scope", "read write")
		.when()
			.post("/oauth/token")
		.then()
			.log().all(true)
			.statusCode(200)
			.body("access_token", is(not(nullValue())))
			.body("token_type", is("bearer"))
			.body("expires_in", is(lessThan(3600)))
			.body("scope", is("read write"));
	}
	
	@Test
	public void check_token_endpoint_is_working() {
		String token = given()
			.auth().basic("restaurant-monolith", "123")
			.param("grant_type", "password")
			.param("username", "waiter")
			.param("password", "123")
			.param("scope", "read write")
		.when()
			.post("/oauth/token")
		.then()
			.extract().body().path("access_token");
		
		given()
			.auth().basic("restaurant-monolith", "123")
			.param("token", token)
		.when()
			.get("/oauth/check_token")
		.then()
			.log().all(true);
	}

}
