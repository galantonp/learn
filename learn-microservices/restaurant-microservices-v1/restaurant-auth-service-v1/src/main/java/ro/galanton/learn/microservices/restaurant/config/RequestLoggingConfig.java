package ro.galanton.learn.microservices.restaurant.config;

import org.apache.catalina.filters.RequestDumperFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RequestLoggingConfig {

	@Bean
	public FilterRegistrationBean requestLoggingFilter() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		RequestDumperFilter filter = new RequestDumperFilter();
		registration.setFilter(filter);
		registration.addUrlPatterns("/*");
		return registration;
	}

}
