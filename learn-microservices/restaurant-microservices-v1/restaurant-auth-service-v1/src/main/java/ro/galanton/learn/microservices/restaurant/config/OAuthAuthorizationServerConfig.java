package ro.galanton.learn.microservices.restaurant.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

@Configuration
@Profile("oauth")
public class OAuthAuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	// see: http://projects.spring.io/spring-security-oauth/docs/oauth2.html
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints
			.authenticationManager(authenticationManager) // required for 'password' grant type
			.tokenStore(tokenStore());
	}
	
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
			.withClient("restaurant-monolith")
				.secret("123")
				.authorizedGrantTypes("password")
				.accessTokenValiditySeconds(3600)
				.scopes("read", "write"); // at least a scope is needed
	}
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security
//			.checkTokenAccess("permitAll()"); // allow unauthenticated access to /oauth/check_token
//			.checkTokenAccess("denyAll()"); // deny access to /oauth/check_token
			.checkTokenAccess("isAuthenticated()"); // allow only authenticated access to /oauth/check_token
		
		// for other expression-based access control, see:
		// http://docs.spring.io/spring-security/site/docs/3.2.5.RELEASE/reference/htmlsingle/#el-access
	}

	@Bean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}
}
