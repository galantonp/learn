package ro.galanton.learn.microservices.restaurant.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@Profile("oauth")
public class OAuthResourceServerConfig extends ResourceServerConfigurerAdapter {

	// see: http://projects.spring.io/spring-security-oauth/docs/oauth2.html 
	
//	@Override
//	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//		resources
//			.stateless(true);
//	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		// see: https://github.com/spring-projects/spring-boot/issues/4332
		http
			.authorizeRequests()
				.anyRequest().authenticated();
				
	}
	
}
