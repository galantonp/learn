package ro.galanton.learn.microservices.restaurant.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.access.annotation.Secured;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ro.galanton.learn.microservices.restaurant.model.Table;
import ro.galanton.learn.microservices.restaurant.resource.TableResource;
import ro.galanton.learn.microservices.restaurant.service.TableService;

@RestController
public class TablesController {

	// NOTE: this is not a completely RESTful controller
	// it is just a simple conversion of the v0 controller with the UI stuff removed
	
	private final TableService tableService;

	public TablesController(TableService tableService) {
		this.tableService = tableService;
	}
	
	@GetMapping("/tables")
	public List<TableResource> list() {
		Iterable<Table> tables = tableService.list();

		List<TableResource> result = new ArrayList<>();
		for (Table table : tables) {
			result.add(new TableResource(table));
		}
		
		return result;
	}
	
	@Secured("ROLE_MANAGER")
	@PostMapping(value = "/tables")
	public TableResource create(@RequestParam("number") int number, @RequestParam("seats") int seats) {
		// TODO: handle exception when table number already exists
		return new TableResource(tableService.create(number, seats));
	}
	
	@Secured("ROLE_MANAGER")
	@DeleteMapping(value = "/tables/{number}")
	public void delete(@PathVariable("number") int number, Model model) {
		tableService.delete(number);
	}
	
}
