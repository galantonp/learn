package ro.galanton.learn.microservices.restaurant.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ro.galanton.learn.microservices.restaurant.model.Order;
import ro.galanton.learn.microservices.restaurant.resource.OrderResource;
import ro.galanton.learn.microservices.restaurant.service.OrderService;

@RestController
@Secured("ROLE_WAITER")
public class OrdersController {

	// NOTE: this is not a completely RESTful controller
	// it is just a simple conversion of the v0 controller with the UI stuff removed
	
	private final OrderService orderService;

	public OrdersController(OrderService orderService) {
		this.orderService = orderService;
	}
	
	@GetMapping("/orders")
	public List<OrderResource> listAll() {
		Iterable<Order> orders = orderService.list();
		
		List<OrderResource> result = new ArrayList<>();
		for (Order order : orders) {
			result.add(new OrderResource(order));
		}
		
		return result;
	}
	
	@PostMapping(value = "/orders")
	public OrderResource newOrder(@RequestParam("tableNumber") int tableNumber) {
		return new OrderResource(orderService.create(tableNumber));
	}
	
	@GetMapping("/orders/{tableNumber}")
	public OrderResource orderDetails(@PathVariable("tableNumber") int tableNumber) {
		return new OrderResource(orderService.findByTableNumber(tableNumber));
	}
	
	@PostMapping(value="/orders/{orderId}")
	public OrderResource addEntry(@PathVariable("orderId") int orderId, @RequestParam("dish") int dishId, @RequestParam("amount") int amount) {
		return new OrderResource(orderService.addDish(orderId, dishId, amount));
	}
	
	@DeleteMapping(value = "/orders/{orderId}")
	public void finishOrder(@PathVariable("orderId") int orderId) {
		orderService.finishOrder(orderId);
	}
}
