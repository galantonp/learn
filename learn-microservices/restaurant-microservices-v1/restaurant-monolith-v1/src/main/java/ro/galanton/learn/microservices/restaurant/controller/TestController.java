package ro.galanton.learn.microservices.restaurant.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/test_waiter")
	@Secured("ROLE_WAITER")
	public String testWaiter() {
		System.out.println("### test waiter");
		return "test waiter";
	}
	
	@GetMapping("/test_manager")
	@Secured("ROLE_MANAGER")
	public String testManager() {
		System.out.println("### test manager");
		return "test manager";
	}

}
