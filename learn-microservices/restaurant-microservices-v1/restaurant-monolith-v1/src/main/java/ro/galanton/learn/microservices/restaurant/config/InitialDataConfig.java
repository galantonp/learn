package ro.galanton.learn.microservices.restaurant.config;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import ro.galanton.learn.microservices.restaurant.service.DishService;
import ro.galanton.learn.microservices.restaurant.service.TableService;

@Configuration
public class InitialDataConfig {

	@Autowired
	public void initialTables(TableService service) {
		service.create(1, 5);
		service.create(2, 7);
	}
	
	@Autowired
	public void initialDishes(DishService service) {
		service.create("Pizza Canibale", "...", BigDecimal.valueOf(10));
		service.create("Pizza Quattro Stagioni", "...", BigDecimal.valueOf(8));
	}

}
