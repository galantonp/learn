package ro.galanton.learn.microservices.restaurant.resource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ro.galanton.learn.microservices.restaurant.model.Order;
import ro.galanton.learn.microservices.restaurant.model.OrderEntry;

public class OrderResource {

	private int id;
	private int tableNumber;
	private BigDecimal total;
	private List<OrderEntryResource> entries;

	public OrderResource() {
	}

	public OrderResource(Order order) {
		this.id = order.getId();
		this.tableNumber = order.getTable().getNumber();
		this.total = order.getTotal();
		this.entries = new ArrayList<>();
		for (OrderEntry entry : order.getEntries()) {
			entries.add(new OrderEntryResource(entry));
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTableNumber() {
		return tableNumber;
	}

	public void setTableNumber(int tableNumber) {
		this.tableNumber = tableNumber;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<OrderEntryResource> getEntries() {
		return entries;
	}

	public void setEntries(List<OrderEntryResource> entries) {
		this.entries = entries;
	}
	
}
