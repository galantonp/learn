package ro.galanton.learn.microservices.restaurant.resource;

import ro.galanton.learn.microservices.restaurant.model.Table;

public class TableResource {

	private int number;
	private int seats;
	private int currentOrderId;

	public TableResource() {
	}

	public TableResource(Table table) {
		this.number = table.getNumber();
		this.seats = table.getSeats();
		if (table.getCurrentOrder() != null) {
			this.currentOrderId = table.getCurrentOrder().getId();
		}
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public int getCurrentOrderId() {
		return currentOrderId;
	}

	public void setCurrentOrderId(int currentOrderId) {
		this.currentOrderId = currentOrderId;
	}
	
}
