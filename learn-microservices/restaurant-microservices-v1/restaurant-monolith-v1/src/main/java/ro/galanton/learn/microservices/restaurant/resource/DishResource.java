package ro.galanton.learn.microservices.restaurant.resource;

import java.math.BigDecimal;

import ro.galanton.learn.microservices.restaurant.model.Dish;

public class DishResource {

	private int id;
	private String name;
	private BigDecimal price;
	private String description;

	public DishResource() {
	}

	public DishResource(Dish dish) {
		this.id = dish.getId();
		this.name = dish.getName();
		this.price = dish.getPrice();
		this.description = dish.getDescription();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
