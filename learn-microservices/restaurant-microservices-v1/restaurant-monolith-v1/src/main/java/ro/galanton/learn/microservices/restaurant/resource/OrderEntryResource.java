package ro.galanton.learn.microservices.restaurant.resource;

import java.math.BigDecimal;

import ro.galanton.learn.microservices.restaurant.model.OrderEntry;

public class OrderEntryResource {

	private int id;
	private DishResource dish;
	private int amount;
	private BigDecimal subtotal;

	public OrderEntryResource() {
	}

	public OrderEntryResource(OrderEntry entry) {
		this.id = entry.getId();
		this.dish = new DishResource(entry.getDish());
		this.amount = entry.getAmount();
		this.subtotal = entry.getSubtotal();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public DishResource getDish() {
		return dish;
	}

	public void setDish(DishResource dish) {
		this.dish = dish;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	
}
