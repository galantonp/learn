package ro.galanton.yonder.attacker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.galanton.yonder.attacker.model.StolenCookies;

public interface StolenCookiesRepository extends JpaRepository<StolenCookies, Long> {

}
