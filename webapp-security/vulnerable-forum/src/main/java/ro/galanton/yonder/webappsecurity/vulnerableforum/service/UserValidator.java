package ro.galanton.yonder.webappsecurity.vulnerableforum.service;

import org.springframework.stereotype.Component;

import ro.galanton.yonder.webappsecurity.vulnerableforum.model.User;

@Component
final class UserValidator {

	public void validate(User user) throws ValidationException {
		if (user == null) {
			throw new ValidationException("No user provided");
		}
		
		if (user.getUsername() == null || user.getUsername().isEmpty()) {
			throw new ValidationException("Username is mandatory");
		}
		
		if (user.getPassword() == null || user.getPassword().isEmpty()) {
			throw new ValidationException("Password is mandatory");
		}
	}
	
}
