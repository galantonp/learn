package ro.galanton.yonder.webappsecurity.vulnerableforum.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;

public abstract class BaseController {

	@ExceptionHandler(Exception.class)
	public void handleException(Exception e) {
		System.err.println(e.getMessage());
		e.printStackTrace(System.err);
	}
}
