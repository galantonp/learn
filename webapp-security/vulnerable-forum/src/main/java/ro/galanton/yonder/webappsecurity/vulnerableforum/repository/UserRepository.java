package ro.galanton.yonder.webappsecurity.vulnerableforum.repository;

import org.springframework.data.repository.CrudRepository;

import ro.galanton.yonder.webappsecurity.vulnerableforum.model.User;

public interface UserRepository extends CrudRepository<User, String> {
	
}