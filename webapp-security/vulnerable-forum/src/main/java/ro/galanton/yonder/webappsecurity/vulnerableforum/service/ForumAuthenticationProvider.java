package ro.galanton.yonder.webappsecurity.vulnerableforum.service;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ro.galanton.yonder.webappsecurity.vulnerableforum.model.User;
import ro.galanton.yonder.webappsecurity.vulnerableforum.repository.UserRepository;

@Service
public class ForumAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserRepository repository;
	
	
	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		String username = String.valueOf(auth.getPrincipal());
		String password = String.valueOf(auth.getCredentials());
		
		User user = repository.findOne(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password");
		}
		
		if (!user.getPassword().equals(password)) {
			throw new BadCredentialsException("Invalid username or password");
		}
		
		return new UsernamePasswordAuthenticationToken(username, null, Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
	}

	@Override
	public boolean supports(Class<?> cls) {
		return cls.equals(UsernamePasswordAuthenticationToken.class);
	}

}
