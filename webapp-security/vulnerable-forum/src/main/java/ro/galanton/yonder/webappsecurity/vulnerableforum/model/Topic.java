package ro.galanton.yonder.webappsecurity.vulnerableforum.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Topic {

	@Id
	@GeneratedValue
	private Long id;

	private String title;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar date;

	@Lob
	private String content;
	
	@OneToOne
	private User author;

	@OneToMany(cascade = CascadeType.ALL, mappedBy="topic")
	@OrderBy("date")
	private List<Comment> comments = new ArrayList<>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public Calendar getDate() {
		return date;
	}
	
	public void setDate(Calendar date) {
		this.date = date;
	}
	
	public User getAuthor() {
		return author;
	}
	
	public void setAuthor(User createdBy) {
		this.author = createdBy;
	}
	
	public List<Comment> getComments() {
		return comments;
	}
	
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

}
