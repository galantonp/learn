package ro.galanton.yonder.webappsecurity.vulnerableforum.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.galanton.yonder.webappsecurity.vulnerableforum.model.User;
import ro.galanton.yonder.webappsecurity.vulnerableforum.repository.UserRepository;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository repository;
	
	@Autowired
	private UserValidator validator;
	
	public void register(String username, String password) throws ValidationException {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		// TODO use BCryptPasswordEncoder once spring security is added
		
		validator.validate(user);
		if (repository.exists(username)) {
			throw new ValidationException(String.format("Username %s is already registered", username));
		}
		
		repository.save(user);
	}
	
	public void changePassword(String username, String password) throws ValidationException {
		User user = repository.findOne(username);
		if (user == null) {
			throw new ValidationException("Invalid username");
		}
		
		user.setPassword(password);
		
		validator.validate(user);
		repository.save(user);
	}
	
}
