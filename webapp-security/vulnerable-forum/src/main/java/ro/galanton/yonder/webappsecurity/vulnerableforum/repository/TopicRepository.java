package ro.galanton.yonder.webappsecurity.vulnerableforum.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ro.galanton.yonder.webappsecurity.vulnerableforum.model.Topic;

public interface TopicRepository extends JpaRepository<Topic, Long> {

	@Query("SELECT t FROM Topic t ORDER BY date DESC")
	Iterable<Topic> list();

}
