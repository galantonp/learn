package ro.galanton.yonder.webappsecurity.vulnerableforum.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ro.galanton.yonder.webappsecurity.vulnerableforum.Application;
import ro.galanton.yonder.webappsecurity.vulnerableforum.model.User;
import ro.galanton.yonder.webappsecurity.vulnerableforum.repository.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class UserServiceIntegrationTest {

	@Autowired
	private UserService service;
	
	@Autowired
	private UserRepository repository;
	
	@Test
	public void it_registers_new_users() throws Exception {
		service.register("petru", "123");
		
		User user = repository.findOne("petru");
		assertNotNull(user);
		assertEquals("petru", user.getUsername());
	}
	
	@Test(expected = ValidationException.class)
	public void username_may_not_be_empty() throws Exception {
		service.register("", "123");
	}
	
	@Test(expected = ValidationException.class)
	public void username_may_not_be_null() throws Exception {
		service.register(null, "123");
	}
	
	@Test(expected = ValidationException.class)
	public void password_may_not_be_empty() throws Exception {
		service.register("petru", "");
	}
	
	@Test(expected = ValidationException.class)
	public void password_may_not_be_null() throws Exception {
		service.register("petru", null);
	}
	
	@Test(expected = ValidationException.class)
	public void username_must_be_unique() throws Exception {
		service.register("petru", "123");
		service.register("petru", "456");
	}
}
