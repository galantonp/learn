package ro.galanton.learn.jdbc.steps;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import ro.galanton.learn.jdbc.TransactionStep;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * A transaction step which causes the current step to wait for a {@link CyclicBarrier}. This kind 
 * of transaction steps can be used to cause concurrent transactions to wait for eachother in 
 * order to help demonstrate transaction anomalies.
 * 
 * @author Petru Galanton
 */
public class WaitForBarrier extends AbstractStep implements TransactionStep {

	private static final Logger log = LoggerFactory.getLogger(WaitForBarrier.class);
	
	private CyclicBarrier barrier;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param txName TODO documentation
	 * @param barrier the barrier on which this step's thread must wait when this step is executed
	 */
	public WaitForBarrier(String txName, CyclicBarrier barrier) {
		super(txName);
		this.barrier = barrier;
	}
	
	@Override
	public void execute(Connection con) throws SQLException {
		log.info(toString());
		
		try {
			barrier.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String toString() {
		return "WaitForBarrier [txName=" + getTransactionName() + ", barrier=" + barrier + "]";
	}

}
