package ro.galanton.learn.jdbc.logging;

/**
 * A {@link Logger} which logs to the standard output.
 * 
 * @author Petru Galanton
 */
public class StdOutLogger implements Logger {

	@SuppressWarnings("unused")
	private String source;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param source the source of the messages that will be logged through this logger.
	 */
	public StdOutLogger(Class<?> source) {
		this.source = source.getName();
	}
	
	
	@Override
	public void info(String msg) {
		info(msg, new Object[0]);
	}
	
	@Override
	public void info(String msg, Object... args) {
		System.out.println("[INFO]  " + format(msg, args));
	}

	@Override
	public void error(Throwable e, String msg, Object... args) {
		System.out.println("[ERROR] " + format(msg, args));
		System.out.println("[ERROR] " + e.getMessage());
		e.printStackTrace(System.out);
	}

	private String format(String msg, Object... args) {
		return String.format(msg, args);
	}
}
