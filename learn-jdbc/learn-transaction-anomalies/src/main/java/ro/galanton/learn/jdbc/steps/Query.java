package ro.galanton.learn.jdbc.steps;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ro.galanton.learn.jdbc.TransactionStep;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * A transaction step which executes a query and stores the returned rows. Assertions on the 
 * returned rows can later be made with {@link Expect} transaction steps.
 * 
 * @author Petru Galanton
 */
public class Query extends AbstractStep implements TransactionStep {

	private static final Logger log = LoggerFactory.getLogger(Query.class);
	
	private String sql;
	private Object[] parameters;
	private List<Object[]> rows = new ArrayList<>();
	
	/**
	 * Creates a new instance.
	 * 
	 * @param txName TODO documentation
	 * @param sql the SQL query string (can be parameterized)
	 * @param parameters the query parameters
	 */
	public Query(String txName, String sql, Object[] parameters) {
		super(txName);
		this.sql = sql;
		this.parameters = parameters;
	}
	
	@Override
	public void execute(Connection con) throws SQLException {
		
		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			for (int i = 0; i < parameters.length; i++) {
				stmt.setObject(i+1, parameters[i]);
			}
			
			log.info("starting " + toString());
			ResultSet rs = stmt.executeQuery();
			log.info("completed " + toString());
			
			int columnCount = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				Object[] row = new Object[columnCount];
				for (int i = 0; i < columnCount; i++) {
					row[i] = rs.getObject(i+1);
				}
				
				rows.add(row);
			}
		} catch (SQLException e) {
			log.error(e, "exception in " + toString());
			throw e;
		}
	}
	
	/**
	 * Returns the rows returned by this query.
	 * 
	 * @return the rows returned by this query (never null).
	 */
	public List<Object[]> getRows() {
		return rows;
	}

	@Override
	public String toString() {
		return "Query [txName=" + getTransactionName() + ", sql=" + sql + ", parameters=" + Arrays.toString(parameters) + ", rows=" + rows + "]";
	}

	
}
