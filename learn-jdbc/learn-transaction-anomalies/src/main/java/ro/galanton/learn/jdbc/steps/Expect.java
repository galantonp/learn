package ro.galanton.learn.jdbc.steps;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import ro.galanton.learn.jdbc.TransactionStep;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * A transaction step which is used do define expectations/assertions on the results returned by 
 * a {@link Query} transaction step.
 * 
 * @author Petru Galanton
 */
public class Expect extends AbstractStep implements TransactionStep {

	private static final Logger log = LoggerFactory.getLogger(Expect.class);
	
	private Query query;
	private int rowNumber;
	private Object[] expectedValues;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param txName TODO documentation
	 * @param query the query on whose results the assertions are going to be made
	 * @param rowNumber the row number on which the assertions are going to be made
	 * @param expectedValues the expected values returned by {@code query} on row {@code rowNumber}.
	 */
	public Expect(String txName, Query query, int rowNumber, Object[] expectedValues) {
		super(txName);
		this.query = query;
		this.rowNumber = rowNumber;
		this.expectedValues = expectedValues;
	}
	
	@Override
	public void execute(Connection con) throws SQLException {
		log.info(toString());
		
		List<Object[]> rows = query.getRows();
		
		if (rows.isEmpty()) {
			throw new IllegalArgumentException(query + " did not return row number " + rowNumber);
		}
		Object[] row = rows.get(rowNumber);
		
		if (row.length != expectedValues.length) {
			throw new IllegalArgumentException(query + " did not return expected column number " + expectedValues.length);
		}
		
		for (int i = 0; i < expectedValues.length; i++) {
			if (!Objects.equals(row[i], expectedValues[i])) {
				throw new IllegalArgumentException("Value " + row[i] + " on column " + i + " did not match expected value " + expectedValues[i]);
			}
		}
	}

	@Override
	public String toString() {
		return "Expect [txName=" + getTransactionName() + ", rowNumber=" + rowNumber + ", expectedValues=" + Arrays.toString(expectedValues) + "]";
	}

}
