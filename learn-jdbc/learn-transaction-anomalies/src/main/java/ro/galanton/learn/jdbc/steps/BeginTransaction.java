package ro.galanton.learn.jdbc.steps;

import java.sql.Connection;
import java.sql.SQLException;

import ro.galanton.learn.jdbc.TransactionStep;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * A transaction steps which begins a transaction with a specified isolation level. 
 * 
 * @author Petru Galanton
 */
public class BeginTransaction extends AbstractStep implements TransactionStep {

	private static final Logger log = LoggerFactory.getLogger(BeginTransaction.class);
	
	private int isolation;

	/**
	 * Creates a new Begin Transaction step.
	 * 
	 * @param isolation the desired transaction isolation level (see constants in {@link Connection})
	 */
	public BeginTransaction(String txName, int isolation) {
		super(txName);
		this.isolation = isolation;
	}
	
	@Override
	public void execute(Connection con) throws SQLException {
		log.info(toString());
		con.setTransactionIsolation(isolation);
		con.setAutoCommit(false);
	}

	@Override
	public String toString() {
		return "BeginTransaction [txName=" + getTransactionName() + ", isolation=" + isolation + "]";
	}
	
}
