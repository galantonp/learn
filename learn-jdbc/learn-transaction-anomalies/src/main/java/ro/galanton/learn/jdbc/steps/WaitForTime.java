package ro.galanton.learn.jdbc.steps;

import java.sql.Connection;
import java.sql.SQLException;

import ro.galanton.learn.jdbc.TransactionStep;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * A transaction step which causes the current transaction's thread to wait for a specified amount of time.
 * This kind of transaction step can be used to cause transaction steps of two concurrent transactions 
 * to execute in a desired order. Note that depending on whether database locks are being used, using 
 * WaitForTime steps may not yeild the expected step execution order.
 * 
 * @author Petru Galanton
 */
public class WaitForTime extends AbstractStep implements TransactionStep {

	private static final Logger log = LoggerFactory.getLogger(WaitForTime.class);
	
	private long millis;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param txName TODO documentation
	 * @param millis the number of milliseconds this step's thread must wait when this step is executed.
	 */
	public WaitForTime(String txName, long millis) {
		super(txName);
		this.millis = millis;
	}

	@Override
	public void execute(Connection con) throws SQLException {
		log.info(toString());
		
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String toString() {
		return "WaitForTime [txName=" + getTransactionName() + ", millis=" + millis + "]";
	}
	
}
