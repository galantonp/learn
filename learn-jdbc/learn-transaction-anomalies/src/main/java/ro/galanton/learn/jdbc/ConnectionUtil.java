package ro.galanton.learn.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Simplistic connection utilities.
 * 
 * @author Petru Galanton
 */
public class ConnectionUtil {

	private static final String URL = "jdbc:sqlserver://localhost:1433;databaseName=mentoring";
	private static final String USERNAME = "petru";
	private static final String PASSWORD = "123";
	
	/**
	 * Creates and returns a new JDBC connection.
	 * 
	 * @return a new JDBC connection.
	 * @throws SQLException if there is any error creating the connection.
	 */
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}
	
}
