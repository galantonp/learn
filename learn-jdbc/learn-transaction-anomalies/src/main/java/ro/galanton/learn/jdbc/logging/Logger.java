package ro.galanton.learn.jdbc.logging;

/**
 * A logger through which messages can be logged at various verbosity levels. 
 * 
 * @author Petru Galanton
 */
public interface Logger {

	/**
	 * Logs an information message.
	 * 
	 * @param msg the message to be logged.
	 */
	public void info(String msg);
	
	/**
	 * Logs an information message.
	 * 
	 * @param msg the message string (may be a pattern compatible with {@link String#format(String, Object...)}.
	 * @param args optional arguments
	 */
	public void info(String msg, Object... args);
	
	/**
	 * Logs an error message.
	 * 
	 * @param e the exception which caused the error
	 * @param msg the error message (may be a pattern compatible with {@link String#format(String, Object...)}.
	 * @param args optional arguments for the error message.
	 */
	public void error(Throwable e, String msg, Object... args);
	
}
