package ro.galanton.learn.jdbc;

import java.sql.Connection;
import java.util.List;

/**
 * Represents the definition of a transaction as a list of steps in a transaction. The 
 * transaction can be executed by calling the {@link #run()} method. If concurrent execution is desired, 
 * this class implements {@link Runnable} so it may be used in conjunction with {@link Thread#Thread(Runnable)}.
 * 
 * @author Petru Galanton
 */
public class ConcurrentTransaction implements Runnable {

	private List<TransactionStep> steps;
	private Exception exception;

	/**
	 * Creates a new instance.
	 * 
	 * @param steps the steps of this transaction.
	 */
	public ConcurrentTransaction(List<TransactionStep> steps) {
		this.steps = steps;
	}
	
	@Override
	public void run() {
		try (Connection con = ConnectionUtil.getConnection()) {
			for (TransactionStep step : steps) {
				step.execute(con);
			}
		} catch (Exception e) {
			this.exception = e;
		}
	}

	/**
	 * Indicates whether any exception occured during the execution of this transaction.
	 * 
	 * @return {@code true} if an exception occurred, {@code false} otherwise.
	 */
	public boolean hasException() {
		return exception != null;
	}
	
	/**
	 * Returns the exception which occurred during the execution of this transaction, or {@code null} if 
	 * no exception occurred.
	 * 
	 * @return the exception or {@code null}
	 */
	public Exception getException() {
		return exception;
	}
}
