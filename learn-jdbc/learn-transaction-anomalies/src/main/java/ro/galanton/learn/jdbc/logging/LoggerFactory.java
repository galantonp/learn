package ro.galanton.learn.jdbc.logging;

/**
 * Creates {@link Logger} instances.
 * 
 * @author Petru Galanton
 */
public class LoggerFactory {

	public static Logger getLogger(Class<?> cls) {
		return new StdOutLogger(cls);
	}
}
