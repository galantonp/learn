package ro.galanton.learn.jdbc.steps;

/**
 * Base class for transaction steps.
 * 
 * @author Petru Galanton
 */
public abstract class AbstractStep {

	private String txName;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param txName the name of the transaction in which this step is going to be executed.
	 */
	public AbstractStep(String txName) {
		this.txName = txName;
	}
	
	/**
	 * Returns the name of the transaction of this step.
	 * 
	 * @return the name of the transaction
	 */
	public String getTransactionName() {
		return txName;
	}
}
