package ro.galanton.learn.jdbc.tests;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import ro.galanton.learn.jdbc.ConcurrentTransaction;
import ro.galanton.learn.jdbc.ConcurrentTransactionBuilder;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

public class LostUpdateTest extends DatabaseTestBase {

	private static final Logger log = LoggerFactory.getLogger(LostUpdateTest.class);
	
	@Test
	public void with_isolation_read_uncommitted() throws Exception {
		log.info("=== LOST UPDATE WITH ISOLATION READ UNCOMMITTED ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.waitFor(barrier)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 123 + 20, 1)
				.waitFor(1, TimeUnit.SECONDS)
			.commit()
			.waitFor(barrier)
			.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
			.expect("petru", 133) // lost update, tx-2 overwrote the change made by tx-1
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.waitFor(barrier)
				.waitFor(1, TimeUnit.SECONDS)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 123 + 10, 1)
			.commit()
			.waitFor(barrier)
			.build();
		
		execute(tx1, tx2);
	}
	
	@Test
	public void with_isolation_read_committed() throws Exception {
		log.info("=== LOST UPDATE WITH ISOLATION READ UNCOMMITTED ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_COMMITTED)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.waitFor(barrier)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 123 + 20, 1)
				.waitFor(1, TimeUnit.SECONDS)
			.commit()
			.waitFor(barrier)
			.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
			.expect("petru", 133) // lost update, tx-2 overwrote the change made by tx-1
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_COMMITTED)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.waitFor(barrier)
				.waitFor(1, TimeUnit.SECONDS)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 123 + 10, 1)
			.commit()
			.waitFor(barrier)
			.build();
		
		execute(tx1, tx2);
	}
	
	/**
	 * This test case creates a deadlock in the database. One of the transactions is going to be 
	 * killed and an SQLException raised.
	 */
	@Test(expected=SQLException.class)
	public void with_isolation_repetable_read() throws Exception {
		log.info("=== LOST UPDATE WITH ISOLATION REPEATABLE READ ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_REPEATABLE_READ)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.waitFor(barrier)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 123 + 20, 1)
				.waitFor(1, TimeUnit.SECONDS)
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_REPEATABLE_READ)
				.waitFor(1, TimeUnit.SECONDS)
				.query("SELECT name, amount FROM accounts WHERE id = ?", 1)
				.expect("petru", 123)
				.waitFor(barrier)
				.waitFor(1, TimeUnit.SECONDS)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 123 + 10, 1)
			.commit()
			.build();
		
		execute(tx1, tx2);
	}
	
	
}
