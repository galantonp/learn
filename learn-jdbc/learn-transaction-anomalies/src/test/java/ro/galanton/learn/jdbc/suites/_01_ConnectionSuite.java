package ro.galanton.learn.jdbc.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ro.galanton.learn.jdbc.tests.ConcurrentTransactionsTest;
import ro.galanton.learn.jdbc.tests.ConnectionTest;

@RunWith(Suite.class)
@SuiteClasses({
	ConnectionTest.class,
	ConcurrentTransactionsTest.class
})
public class _01_ConnectionSuite {

}
