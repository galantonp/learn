package ro.galanton.learn.jdbc.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ro.galanton.learn.jdbc.tests.Conclusion1Test;
import ro.galanton.learn.jdbc.tests.Conclusion2Test;

@RunWith(Suite.class)
@SuiteClasses({
	Conclusion1Test.class,
	Conclusion2Test.class
})
public class _03_ConclusionsSuite {

}
