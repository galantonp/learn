package ro.galanton.learn.jdbc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ro.galanton.learn.jdbc.ConnectionUtil;

/**
 * Utility class for building a database schema at the beginning of an integration test.
 * 
 * <p>
 * The database schema can be created by executing a list of SQL scripts in order.
 * </p>
 * @author Petru Galanton
 */
public class DatabaseBuilder {

	private List<DatabaseScript> scripts = new ArrayList<>();

	/**
	 * Adds an SQL script to be later executed. The scripts added with this method will be 
	 * executed in the same order in which they have been added.
	 * 
	 * @param script the name of a resource on the classpath which contains an SQL script
	 * @param ignoreErrors whether to ignore errors when this script is going to be executed.
	 * 
	 * @return this builder 
	 * @throws IOException if an error occurs loading the SQL script
	 */
	public DatabaseBuilder script(String script, boolean ignoreErrors) throws IOException {
		StringBuilder builder = new StringBuilder();
		try (BufferedReader reader = getReader(script)) {
			for (;;) {
				String line = reader.readLine();
				if (line == null) break;
				
				builder.append(line);
			}
		}
		
		scripts.add(new DatabaseScript(builder.toString(), ignoreErrors));
		
		return this;
	}
	
	/**
	 * Executes the SQL scripts added to this builder.
	 * @throws SQLException
	 */
	public void execute() throws SQLException {
		try (Connection con = ConnectionUtil.getConnection()) {
			for (DatabaseScript script : scripts) {
				script.execute(con);
			}
		}
	}
	
	private BufferedReader getReader(String script) throws IOException {
		return new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(script)));
	}
	
}
