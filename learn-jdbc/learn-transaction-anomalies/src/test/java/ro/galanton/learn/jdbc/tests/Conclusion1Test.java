package ro.galanton.learn.jdbc.tests;

import java.sql.Connection;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import ro.galanton.learn.jdbc.ConcurrentTransaction;
import ro.galanton.learn.jdbc.ConcurrentTransactionBuilder;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

/**
 * Conclusion: once a record has been written by a transaction tx-1, transaction tx-2 at 
 * isolation level read committed cannot read it until tx-1 has finished. It does not matter what 
 * was the isolation level of tx-1.
 * 
 * @author Petru Galanton
 */
@RunWith(Parameterized.class)
public class Conclusion1Test extends DatabaseTestBase {

	private static final Logger log = LoggerFactory.getLogger(Conclusion1Test.class);
	
	@Parameters
	public static Object[][] parameters() {
		return new Object[][] { 
			{ Connection.TRANSACTION_READ_UNCOMMITTED },
			{ Connection.TRANSACTION_READ_COMMITTED },
			{ Connection.TRANSACTION_REPEATABLE_READ },
			{ Connection.TRANSACTION_SERIALIZABLE }
		};
	}
	
	@Parameter
	public int tx1Isolation;
	
	@Test
	public void test() throws Exception {
		log.info("=== CONCLUSION 1, TX-1 ISOLATION: " + tx1Isolation + " ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.begin(tx1Isolation)
				.waitFor(barrier)
				.update("UPDATE accounts SET amount = ? WHERE id = ?", 200, 1)
				.waitFor(2, TimeUnit.SECONDS)
				.query("SELECT amount FROM accounts WHERE id = ?", 1)
				.expect(200)
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.begin(Connection.TRANSACTION_READ_COMMITTED)
				.waitFor(barrier)
				.waitFor(1, TimeUnit.SECONDS)
				.query("SELECT amount FROM accounts WHERE id = ?", 1)
				.expect(200)
			.commit()
			.build();
		
		execute(tx1, tx2);
	}
}
