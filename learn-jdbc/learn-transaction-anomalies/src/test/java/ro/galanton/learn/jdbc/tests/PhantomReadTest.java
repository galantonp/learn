package ro.galanton.learn.jdbc.tests;

import java.sql.Connection;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import ro.galanton.learn.jdbc.ConcurrentTransaction;
import ro.galanton.learn.jdbc.ConcurrentTransactionBuilder;
import ro.galanton.learn.jdbc.logging.Logger;
import ro.galanton.learn.jdbc.logging.LoggerFactory;

public class PhantomReadTest extends DatabaseTestBase {

	private static final Logger log = LoggerFactory.getLogger(PhantomReadTest.class);
	
	@Test
	public void with_isolation_repeatable_read() throws Exception {
		log.info("=== PHANTOM READ WITH ISOLATION REPEATABLE READ ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_REPEATABLE_READ)
				.query("SELECT name, amount FROM accounts ORDER BY amount")
				.expect("petru", 123)
				.expect("paul", 456)
				.waitFor(2, TimeUnit.SECONDS)
				.query("SELECT name, amount FROM accounts ORDER BY amount")
				.expect("petru", 123)
				.expect("paul", 456)
				.expect("vasile", 789) // phantom read
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.waitFor(1, TimeUnit.SECONDS)
				.update("INSERT INTO accounts (id, name, amount) VALUES (?, ?, ?)", 3, "vasile", 789)
			.commit()
			.build();
		
		execute(tx1, tx2);
	}
	
	@Test
	public void with_isolation_serializable() throws Exception {
		log.info("=== PHANTOM READ WITH ISOLATION SERIALIZABLE ===");
		
		CyclicBarrier barrier = new CyclicBarrier(2);
		
		ConcurrentTransaction tx1 = new ConcurrentTransactionBuilder("tx-1")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_SERIALIZABLE)
				.query("SELECT name, amount FROM accounts ORDER BY amount")
				.expect("petru", 123)
				.expect("paul", 456)
				.waitFor(2, TimeUnit.SECONDS)
				.query("SELECT name, amount FROM accounts ORDER BY amount")
				.expect("petru", 123)
				.expect("paul", 456) // no more phantom read
			.commit()
			.build();
		
		ConcurrentTransaction tx2 = new ConcurrentTransactionBuilder("tx-2")
			.waitFor(barrier)
			.begin(Connection.TRANSACTION_READ_UNCOMMITTED)
				.waitFor(1, TimeUnit.SECONDS)
				.update("INSERT INTO accounts (id, name, amount) VALUES (?, ?, ?)", 3, "vasile", 789)
			.commit()
			.build();
		
		execute(tx1, tx2);
	}
}
