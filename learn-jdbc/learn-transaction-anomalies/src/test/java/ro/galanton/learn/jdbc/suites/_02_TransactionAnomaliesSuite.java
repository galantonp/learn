package ro.galanton.learn.jdbc.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ro.galanton.learn.jdbc.tests.DirtyReadTest;
import ro.galanton.learn.jdbc.tests.LostUpdateTest;
import ro.galanton.learn.jdbc.tests.NonRepeatableReadTest;
import ro.galanton.learn.jdbc.tests.PhantomReadTest;

@RunWith(Suite.class)
@SuiteClasses({
	DirtyReadTest.class,
	NonRepeatableReadTest.class,
	PhantomReadTest.class,
	LostUpdateTest.class
})
public class _02_TransactionAnomaliesSuite {

}
