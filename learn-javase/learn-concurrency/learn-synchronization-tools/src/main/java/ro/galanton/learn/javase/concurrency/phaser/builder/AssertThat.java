package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import org.hamcrest.Matcher;

import ro.galanton.learn.javase.concurrency.phaser.PhaserOperation;
import ro.galanton.learn.javase.concurrency.util.worker.builder.AbstractAssertThat;
import ro.galanton.learn.javase.concurrency.util.worker.builder.DelayedValue;

final class AssertThat<T> extends AbstractAssertThat<T, Phaser> implements PhaserOperation {

	public AssertThat(DelayedValue<T> actual, Matcher<T> matcher) {
		super(actual, matcher);
	}
	
	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("AssertThat does not return an int result");
	}
	
}
