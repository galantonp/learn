package ro.galanton.learn.javase.concurrency.util.worker.builder;

import ro.galanton.learn.javase.concurrency.util.Logger;

public class AbstractSleep<T> extends AbstractOperation<T> {

	private final long millis;
	
	public AbstractSleep(long millis) {
		this.millis = millis;
	}
	
	@Override
	protected void doExecute(T syncTool, Logger logger) throws Exception {
		logger.log("before sleep(%d)", millis);
		Thread.sleep(millis);
		logger.log("after sleep(%d)", millis);
	}

}
