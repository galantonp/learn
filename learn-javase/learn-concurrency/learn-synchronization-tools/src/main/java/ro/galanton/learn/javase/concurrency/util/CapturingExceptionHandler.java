package ro.galanton.learn.javase.concurrency.util;

public class CapturingExceptionHandler implements ExceptionHandler {

	private Throwable capturedException;
	
	@Override
	public void handleException(Throwable e) {
		this.capturedException = e;
	}

	public Throwable getCapturedException() {
		return capturedException;
	}
}
