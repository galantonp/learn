package ro.galanton.learn.javase.concurrency.util.worker.builder;

@FunctionalInterface
public interface Statement {

	public void execute();
	
}
