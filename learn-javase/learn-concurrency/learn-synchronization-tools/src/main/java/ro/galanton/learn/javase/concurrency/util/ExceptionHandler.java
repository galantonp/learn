package ro.galanton.learn.javase.concurrency.util;

public interface ExceptionHandler {

	void handleException(Throwable e);
	
}
