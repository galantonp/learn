package ro.galanton.learn.javase.concurrency.util.worker.builder;

@FunctionalInterface
public interface DelayedValue<T> {

	public T evaluate();
}
