package ro.galanton.learn.javase.concurrency.util.worker.builder;

import static org.hamcrest.MatcherAssert.assertThat;

import org.hamcrest.Matcher;

import ro.galanton.learn.javase.concurrency.util.Logger;

/**
 * @author Petru Galanton
 *
 * @param <T>
 *            the type that is being asserted on
 * @param <V>
 *            the syncronization tool type
 */
public abstract class AbstractAssertThat<T, V> extends AbstractOperation<V> implements Assertion {

	private final DelayedValue<T> actual;
	private final Matcher<T> matcher;

	private AssertionError error;

	public AbstractAssertThat(DelayedValue<T> actual, Matcher<T> matcher) {
		this.actual = actual;
		this.matcher = matcher;
	}

	@Override
	protected void doExecute(V syncTool, Logger logger) throws Exception {
		try {
			T actualValue = actual.evaluate();
			logger.log("assert that %s %s", actualValue, matcher);

			assertThat(actualValue, matcher);
		} catch (AssertionError e) {
			this.error = e;
			throw e;
		}
	}

	@Override
	public AssertionError getAssertionError() {
		return error;
	}

}
