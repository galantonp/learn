package ro.galanton.learn.javase.concurrency.util.worker;

import ro.galanton.learn.javase.concurrency.util.Logger;

/**
 * An operation on a synchronization tool.
 * 
 * @author Petru Galanton
 *
 * @param <T>
 *            the synchronization tool
 */
public interface Operation<T> {

	void execute(T synchTool, Logger logger) throws Exception;
	
	boolean isStarted();
	
	boolean isFinished();

	Throwable getThrowable();
	
}
