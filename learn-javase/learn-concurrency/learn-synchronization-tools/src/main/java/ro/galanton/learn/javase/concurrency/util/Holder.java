package ro.galanton.learn.javase.concurrency.util;

public class Holder<T> {

	private T value;
	
	public Holder(T initialValue) {
		this.value = initialValue;
	}
	
	public T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
}
