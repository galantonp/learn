package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.Map;
import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.phaser.PhaserOperation;
import ro.galanton.learn.javase.concurrency.util.worker.builder.AbstractCapture;
import ro.galanton.learn.javase.concurrency.util.worker.builder.DelayedValue;

final class Capture<K, V> extends AbstractCapture<K, V, Phaser> implements PhaserOperation {

	
	public Capture(Map<K, ? super V> map, K key, DelayedValue<V> actual) {
		super(map, key, actual);
	}
	
	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("Capture operation does not return an int result");
	}

}
