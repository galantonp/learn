package ro.galanton.learn.javase.concurrency.util;

public class ThreadNameLogger implements Logger {

	private final Logger delegate;
	
	public ThreadNameLogger(Logger delegate) {
		this.delegate = delegate;
	}
	
	@Override
	public void log(String message) {
		String threadName = Thread.currentThread().getName();
		delegate.log(String.format("%s: %s", threadName, message));
	}

	@Override
	public void log(String format, Object... args) {
		log(String.format(format, args));
	}
	
}
