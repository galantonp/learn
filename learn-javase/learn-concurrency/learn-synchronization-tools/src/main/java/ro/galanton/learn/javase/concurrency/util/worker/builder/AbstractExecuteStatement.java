package ro.galanton.learn.javase.concurrency.util.worker.builder;

import ro.galanton.learn.javase.concurrency.util.Logger;

public abstract class AbstractExecuteStatement<T> extends AbstractOperation<T> {

	private final Statement stmt;
	
	public AbstractExecuteStatement(Statement stmt) {
		this.stmt = stmt;
	}
	
	@Override
	protected void doExecute(T syncTool, Logger logger) throws Exception {
		logger.log("before statement");
		stmt.execute();
		logger.log("after statement");
	}

}
