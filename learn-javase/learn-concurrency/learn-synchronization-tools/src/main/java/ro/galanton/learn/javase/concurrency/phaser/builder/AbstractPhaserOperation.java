package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.phaser.PhaserOperation;
import ro.galanton.learn.javase.concurrency.util.worker.builder.AbstractOperation;

abstract class AbstractPhaserOperation extends AbstractOperation<Phaser> implements PhaserOperation {

	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("AssertThat does not return an int result");
	}
	
}
