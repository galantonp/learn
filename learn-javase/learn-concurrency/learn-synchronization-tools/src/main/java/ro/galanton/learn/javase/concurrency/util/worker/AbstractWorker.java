package ro.galanton.learn.javase.concurrency.util.worker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import ro.galanton.learn.javase.concurrency.util.ConsoleLogger;
import ro.galanton.learn.javase.concurrency.util.ExceptionHandler;
import ro.galanton.learn.javase.concurrency.util.Logger;
import ro.galanton.learn.javase.concurrency.util.NameLogger;
import ro.galanton.learn.javase.concurrency.util.Named;
import ro.galanton.learn.javase.concurrency.util.ThreadNameLogger;

/**
 * Base class for workers. Helps avoid duplicating a lot of code.
 * 
 * @author Petru Galanton
 *
 * @param <T>
 *            the synchronization tool used by the worker
 * @param <V>
 *            the operation type for the synchronization tool T
 */
public abstract class AbstractWorker<T, V extends Operation<T>> implements Runnable, Named {

	private final T syncTool;
	private final String name;
	private final Optional<ExceptionHandler> exceptionHandler;
	private final Logger logger;

	private List<V> operations = new ArrayList<>();

	public AbstractWorker(T syncTool, String name) {
		this(syncTool, name, null);
	}

	public AbstractWorker(T syncTool, String name, ExceptionHandler exceptionHandler) {
		this.syncTool = syncTool;
		this.name = name;
		this.exceptionHandler = Optional.ofNullable(exceptionHandler);
		this.logger = new ThreadNameLogger(new NameLogger(this, new ConsoleLogger()));
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void run() {
		try {
			for (Operation<T> operation : operations) {
				operation.execute(syncTool, logger);
			}
		} catch (Throwable e) {
			exceptionHandler.ifPresent(handler -> handler.handleException(e));
		}
	}

	public void addOperation(V operation) {
		operations.add(operation);
	}

	public V getOperation(int index) {
		return operations.get(index);
	}

	public List<V> getOperations() {
		return Collections.unmodifiableList(operations);
	}
}
