package ro.galanton.learn.javase.concurrency.barrier;

import java.util.Optional;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import ro.galanton.learn.javase.concurrency.util.ConsoleLogger;
import ro.galanton.learn.javase.concurrency.util.ExceptionHandler;
import ro.galanton.learn.javase.concurrency.util.Logger;
import ro.galanton.learn.javase.concurrency.util.NameLogger;
import ro.galanton.learn.javase.concurrency.util.Named;
import ro.galanton.learn.javase.concurrency.util.ThreadNameLogger;

public class TimedCyclicBarrierWorker implements Runnable, Named {

	private final String name;
	private final CyclicBarrier barrier;
	private final Optional<ExceptionHandler> exceptionHandler;
	private final Logger logger;
	
	private boolean barrierPassed;
	
	public TimedCyclicBarrierWorker(CyclicBarrier barrier, String name) {
		this(barrier, name, null);
	}
	
	public TimedCyclicBarrierWorker(CyclicBarrier barrier, String name, ExceptionHandler exceptionHandler) {
		this.barrier = barrier;
		this.name = name;
		this.exceptionHandler = Optional.ofNullable(exceptionHandler);
		this.logger = new ThreadNameLogger(new NameLogger(this, new ConsoleLogger()));
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void run() {
		try {
			logger.log("waiting at barrier");
			barrier.await(100, TimeUnit.MILLISECONDS);
			logger.log("passed barrier");

			barrierPassed = true;
		} catch (Exception e) {
			if (exceptionHandler.isPresent()) {
				exceptionHandler.get().handleException(e);
			}
		}
	}
	
	public boolean isBarrierPassed() {
		return barrierPassed;
	}
	
}
