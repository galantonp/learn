package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.phaser.PhaserOperation;
import ro.galanton.learn.javase.concurrency.util.worker.builder.AbstractExecuteStatement;
import ro.galanton.learn.javase.concurrency.util.worker.builder.Statement;

final class ExecuteStatement extends AbstractExecuteStatement<Phaser> implements PhaserOperation {

	public ExecuteStatement(Statement stmt) {
		super(stmt);
	}

	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("AssertThat does not return an int result");
	}
	
}
