package ro.galanton.learn.javase.concurrency.util;

public class ConsoleLogger implements Logger {

	@Override
	public void log(String message) {
		System.out.println(message);
	}

	@Override
	public void log(String format, Object... args) {
		log(String.format(format, args));
	}
}
