package ro.galanton.learn.javase.concurrency.util.worker.builder;

import ro.galanton.learn.javase.concurrency.util.Logger;
import ro.galanton.learn.javase.concurrency.util.worker.Operation;

public abstract class AbstractOperation<T> implements Operation<T> {

	private volatile boolean started;
	private volatile boolean finished;
	
	private Throwable throwable;
	
	@Override
	public final void execute(T syncTool, Logger logger) throws Exception {
		started = true;
		try {
			doExecute(syncTool, logger);
		} catch (Throwable e) {
			this.throwable = e;
			throw e;			
		} finally {
			finished = true;
		}
	}
	
	protected abstract void doExecute(T syncTool, Logger logger) throws Exception;

	@Override
	public final boolean isStarted() {
		return started;
	}

	@Override
	public final boolean isFinished() {
		return finished;
	}

	@Override
	public final Throwable getThrowable() {
		return throwable;
	}
	
}
