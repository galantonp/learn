package ro.galanton.learn.javase.concurrency.util.worker.builder;

public interface Assertion {

	AssertionError getAssertionError();
	
}
