package ro.galanton.learn.javase.concurrency.phaser;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.ExceptionHandler;
import ro.galanton.learn.javase.concurrency.util.worker.AbstractWorker;

public class PhaserWorker extends AbstractWorker<Phaser, PhaserOperation> {

	public PhaserWorker(Phaser phaser, String name) {
		this(phaser, name, null);
	}
	
	public PhaserWorker(Phaser phaser, String name, ExceptionHandler exceptionHandler) {
		super(phaser, name, exceptionHandler);
	}
	
}
