package ro.galanton.learn.javase.concurrency.util;

public class SysErrExceptionHandler implements ExceptionHandler {
	
	@Override
	public void handleException(Throwable e) {
		System.err.println(e.getMessage());
		e.printStackTrace(System.err);
	}
	
}
