package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.phaser.PhaserOperation;
import ro.galanton.learn.javase.concurrency.util.worker.builder.AbstractSleep;

final class Sleep extends AbstractSleep<Phaser> implements PhaserOperation {

	public Sleep(long millis) {
		super(millis);
	}
	
	@Override
	public int getIntResult() {
		throw new UnsupportedOperationException("Thread.sleep() does not return an int result");
	}

}
