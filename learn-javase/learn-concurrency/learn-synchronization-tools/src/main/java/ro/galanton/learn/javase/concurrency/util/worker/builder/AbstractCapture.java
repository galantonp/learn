package ro.galanton.learn.javase.concurrency.util.worker.builder;

import java.util.Map;

import ro.galanton.learn.javase.concurrency.util.Logger;

/**
 * 
 * @author Petru Galanton
 *
 * @param <K>
 *            The key type of the captured value
 * @param <V>
 *            The type of the captured value
 * @param <T>
 *            The type of the synchronization tool.
 */
public abstract class AbstractCapture<K, V, T> extends AbstractOperation<T> {

	private Map<K, ? super V> map;
	private K key;
	private DelayedValue<V> actual;

	public AbstractCapture(Map<K, ? super V> map, K key, DelayedValue<V> actual) {
		this.map = map;
		this.key = key;
		this.actual = actual;
	}

	@Override
	public void doExecute(T syncTool, Logger logger) throws Exception {
		map.put(key, actual.evaluate());
	}

}
