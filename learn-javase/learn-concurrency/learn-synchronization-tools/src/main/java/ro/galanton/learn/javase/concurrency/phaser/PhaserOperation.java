package ro.galanton.learn.javase.concurrency.phaser;

import java.util.concurrent.Phaser;

import ro.galanton.learn.javase.concurrency.util.worker.Operation;

public interface PhaserOperation extends Operation<Phaser> {

	int getIntResult();
	
}
