package ro.galanton.learn.javase.concurrency.phaser.builder;

import java.util.Map;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

import org.hamcrest.Matcher;

import ro.galanton.learn.javase.concurrency.phaser.PhaserWorker;
import ro.galanton.learn.javase.concurrency.util.ExceptionHandler;
import ro.galanton.learn.javase.concurrency.util.worker.builder.DelayedValue;
import ro.galanton.learn.javase.concurrency.util.worker.builder.Statement;

public class PhaserWorkerBuilder {

	public static PhaserWorkerBuilder worker(Phaser phaser, String name) {
		return worker(phaser, name, null);
	}
	
	public static PhaserWorkerBuilder worker(Phaser phaser, String name, ExceptionHandler handler) {
		return new PhaserWorkerBuilder(new PhaserWorker(phaser, name, handler));
	}
	
	private final PhaserWorker worker;
	
	public PhaserWorkerBuilder(PhaserWorker worker) {
		this.worker = worker;
	}
	
	public PhaserWorkerBuilder arrive() {
		worker.addOperation(new Arrive());
		return this;
	}
	
	public PhaserWorkerBuilder arriveAndAwaitAdvance() {
		worker.addOperation(new ArriveAndAwaitAdvance());
		return this;
	}
	
	public PhaserWorkerBuilder arriveAndDeregister() {
		worker.addOperation(new ArriveAndDeregister());
		return this;
	}
	
	public PhaserWorkerBuilder awaitAdvance(int phase) {
		worker.addOperation(new AwaitAdvance(phase));
		return this;
	}
	
	public PhaserWorkerBuilder awaitAdvanceInterruptibly(int phase) {
		worker.addOperation(new AwaitAdvanceInterruptibly(phase));
		return this;
	}
	
	public PhaserWorkerBuilder awaitAdvanceInterruptibly(int phase, long timeout, TimeUnit unit) {
		worker.addOperation(new TimedAwaitAdvanceInterruptibly(phase, timeout, unit));
		return this;
	}
	
	public PhaserWorkerBuilder bulkRegister(int parties) {
		worker.addOperation(new BulkRegister(parties));
		return this;
	}
	
	public PhaserWorkerBuilder register() {
		worker.addOperation(new Register());
		return this;
	}
	
	public PhaserWorkerBuilder forceTermination() {
		worker.addOperation(new ForceTermination());
		return this;
	}
	
	public PhaserWorkerBuilder sleep(long millis) {
		worker.addOperation(new Sleep(millis));
		return this;
	}
	
	public <T> PhaserWorkerBuilder assertThat(DelayedValue<T> actual, Matcher<T> matcher) {
		worker.addOperation(new AssertThat<T>(actual, matcher));
		return this;
	}
	
	public <T> CaptureKey<T> capture(DelayedValue<T> actual) {
		return new CaptureKey<T>(this, worker, actual);
	}
	
	public PhaserWorkerBuilder execute(Statement stmt) {
		worker.addOperation(new ExecuteStatement(stmt));
		return this;
	}
	
	public PhaserWorker build() {
		return worker;
	}
	
	public static class CaptureKey<V> {
		
		private PhaserWorkerBuilder builder;
		private PhaserWorker worker;
		private DelayedValue<V> actual;
		
		public CaptureKey(PhaserWorkerBuilder builder, PhaserWorker worker, DelayedValue<V> actual) {
			this.builder = builder;
			this.worker = worker;
			this.actual = actual;
		}
		
		public <K> CaptureValue<K, V> as(K key) {
			return new CaptureValue<K, V>(builder, worker, key, actual);
		}
	}
	
	public static class CaptureValue<K, V> {
		
		private PhaserWorkerBuilder builder;
		private PhaserWorker worker;
		private K key;
		private DelayedValue<V> actual;
		
		public CaptureValue(PhaserWorkerBuilder builder, PhaserWorker worker, K key, DelayedValue<V> actual) {
			this.builder = builder;
			this.worker = worker;
			this.key = key;
			this.actual = actual;
		}
		
		public PhaserWorkerBuilder in(Map<K, ? super V> map) {
			worker.addOperation(new Capture<K, V>(map, key, actual));
			return builder;
		}
	}
}
