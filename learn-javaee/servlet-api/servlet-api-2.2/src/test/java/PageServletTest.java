import junit.framework.Assert;
import junit.framework.TestCase;

public class PageServletTest extends TestCase {

	public PageServletTest(String name) {
		super(name);
	}

	public void testBigbang() throws Exception {
		String response = TestUtils.get("http://localhost:8080/app/page");
		Assert.assertEquals("header menu content footer", response);
	}
}
