import junit.framework.Assert;
import junit.framework.TestCase;

public class ForwardToNamedTest extends TestCase {

	public ForwardToNamedTest(String name) {
		super(name);
	}

	public void testBigBang() throws Exception {
		String response = TestUtils.get("http://localhost:8080/app/forwardToNamed");
		Assert.assertEquals(" content", response);
	}
}
