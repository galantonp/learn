package util;
import java.io.Serializable;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class MyObject implements Serializable, HttpSessionBindingListener {

	private static final long serialVersionUID = 1L;
	
	private String name;
	
	public MyObject(String name) {
		this.name = name;
	}

	public void valueBound(HttpSessionBindingEvent event) {
		System.out.println(this + " was bound to session " + event.getSession().getId());
	}

	public void valueUnbound(HttpSessionBindingEvent event) {
		System.out.println(this + " was unbound from session " + event.getSession().getId());
	}
	
	public String toString() {
		return "MyObject[name=" + name + "]@" + System.identityHashCode(this);
	}

}
