package servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class XmlWebFragmentServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private String registrationMethod;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		this.registrationMethod = config.getInitParameter("registrationMethod");
	}
	
	@Override
	public void init() throws ServletException {
		System.out.println("XmlWebFragmentServlet initialized");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("This is an non-annotated servlet in web fragment xml-web-fragment-3.0 registered " + registrationMethod);
	}
	
}
