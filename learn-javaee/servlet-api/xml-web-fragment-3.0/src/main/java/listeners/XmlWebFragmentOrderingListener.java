package listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class XmlWebFragmentOrderingListener implements ServletContextListener {

	public XmlWebFragmentOrderingListener() {
		System.out.println("OrderingListener in xml-web-fragment instantiated");
	}
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

}
