package servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
	urlPatterns = "/annotatedGreeting",
	initParams = @WebInitParam(name = "greeting", value = "Hello")
)
public class Greeting extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private String greeting;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		this.greeting = config.getInitParameter("greeting");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("name");
		if (name == null || name.isEmpty()) {
			resp.getWriter().println(this.greeting + " stranger!");
		} else {
			resp.getWriter().println(this.greeting + " " + name + "!");
		}
	}
}
