package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/upload")
@MultipartConfig(maxFileSize = Upload.KILO_256, maxRequestSize = Upload.MEGA_1)
public class Upload extends HttpServlet {

	public static final long BYTES_256 = 256;
	public static final long KILO_1 = 1024;
	public static final long KILO_256 = 256 * 1024;
	public static final long MEGA_1 = 1024 * 1024;
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		out.print("<!doctype html>");
		out.print("<html><head></head><body>");
		out.print("<form enctype='multipart/form-data' method='post'>");
		out.print("Some Text: <input type='text' name='someText'><br><br>");
		out.print("File: <input type='file' name='uploadedFile'><br><br>");
		out.print("<input type='submit'>");
		out.print("</form></body></html>");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/plain");
		PrintWriter out = resp.getWriter();

		for (Part part : req.getParts()) {
			out.println("Content-Type: " + part.getContentType());
			out.println("Name: " + part.getName());
			out.println("Size: " + part.getSize() + " bytes");
			for (String header : part.getHeaderNames()) {
				out.println(header + ": " + part.getHeaders(header));
			}
			out.println();
			out.println();
		}
	}
}
