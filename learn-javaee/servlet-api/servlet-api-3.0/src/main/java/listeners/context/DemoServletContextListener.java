package listeners.context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;

import servlets.Greeting;
import util.Constants;
import util.LogUtil;

@WebListener
public class DemoServletContextListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent evt) {
		LogUtil.log("context destroyed: " + evt.getServletContext().getServletContextName());
	}

	public void contextInitialized(ServletContextEvent evt) {
		LogUtil.log("context initialized: " + evt.getServletContext().getServletContextName());
		evt.getServletContext().setAttribute(Constants.CONTEXT_INIT_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
		
		ServletRegistration.Dynamic servlet = evt.getServletContext().addServlet("programmaticGreeting", Greeting.class);
		servlet.addMapping("/programmaticGreeting");
		servlet.setInitParameter("greeting", "What's up");
	}

}
