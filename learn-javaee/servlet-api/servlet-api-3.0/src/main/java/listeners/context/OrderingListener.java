package listeners.context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class OrderingListener implements ServletContextListener {

	public OrderingListener() {
		System.out.println("OrderingListener in servlet-api-3.0 instantiated");
	}
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

}
