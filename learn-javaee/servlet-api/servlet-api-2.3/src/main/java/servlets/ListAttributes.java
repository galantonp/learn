package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.LogUtil;

public class ListAttributes extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LogUtil.log("ListAttributes executing");
		resp.setContentType("text/plain");
		PrintWriter writer = resp.getWriter();
		
		writer.println("Context attributes:");
		Enumeration ctxAttributeNames = getServletContext().getAttributeNames();
		while (ctxAttributeNames.hasMoreElements()) {
			String attrName = (String) ctxAttributeNames.nextElement();
			writer.println("  " + attrName + " = " + getServletContext().getAttribute(attrName));
		}
		writer.println();
		
		writer.println("Session attributes:");
		Enumeration sessionAttributeNames = getServletContext().getAttributeNames();
		while (sessionAttributeNames.hasMoreElements()) {
			String attrName = (String) sessionAttributeNames.nextElement();
			writer.println("  " + attrName + " = " + req.getSession().getAttribute(attrName));
		}
		writer.println();
		
	}
	
}
