package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.LogUtil;

public class AddToContext extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LogUtil.log("AddToContext executing");
		String name = req.getParameter("name");
		String value = req.getParameter("value");
		
		getServletContext().setAttribute(name, value);
	}
}
