package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import util.LogUtil;

public class MappedToPattern implements Filter {

	public void destroy() {
		LogUtil.log("MappedToPattern destroyed");
	}

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		LogUtil.log("MappedToPattern executed");
		chain.doFilter(req, resp);
	}

	public void init(FilterConfig config) throws ServletException {
		LogUtil.log("MappedToPattern initialized");
	}
	
}
