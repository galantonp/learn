package util;

public class LogUtil {

	private static final String nodeName = System.getProperty("node.name");
	
	public static void log(String msg) {
		System.out.println("[" + nodeName + "] " + msg);
	}
	
}
