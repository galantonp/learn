package listeners.session;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import util.LogUtil;

public class DemoHttpSessionListener implements HttpSessionListener {

	// TODO add it in web.xml
	
	public void sessionCreated(HttpSessionEvent evt) {
		LogUtil.log("session with id " + evt.getSession().getId() + " created");
	}

	public void sessionDestroyed(HttpSessionEvent evt) {
		LogUtil.log("session with id " + evt.getSession().getId() + " destroyed");		
	}

}
