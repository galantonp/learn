package ro.galanton.learn.javaee.servletapi;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/")
public class TestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		int count = getCount(session);
		
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello World: " + count);
		
		storeCount(session, count);
	}

	private int getCount(HttpSession session) {
		int count = 0;
		if (session.getAttribute("count") != null) {
			count = (int) session.getAttribute("count");
		}
		
		return count + 1;
	}
	
	private void storeCount(HttpSession session, int count) {
		session.setAttribute("count", count);
	}
}
