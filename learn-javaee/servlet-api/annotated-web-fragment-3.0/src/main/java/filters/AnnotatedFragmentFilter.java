package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter("/foo")
public class AnnotatedFragmentFilter implements Filter {

	public AnnotatedFragmentFilter() {
		System.out.println("AnnotatedFragmentFilter instantiated");
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("AnnotatedFragmentFilter executed");
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}
	
}
