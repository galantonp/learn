Servlet API 2.5 XSD:
<web-app 
	xmlns="http://java.sun.com/xml/ns/javaee" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd" 
	version="2.5">
	
Changes since 2.4:
http://www.javaworld.com/article/2071393/java-web-development/new-features-added-to-servlet-2-5.html?page=2