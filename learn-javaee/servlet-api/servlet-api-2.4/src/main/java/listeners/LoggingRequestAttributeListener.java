package listeners;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;

public class LoggingRequestAttributeListener implements ServletRequestAttributeListener {

	public void attributeAdded(ServletRequestAttributeEvent evt) {
		System.out.println("### attribute added: " + evt.getName() + " = " + evt.getValue());
	}

	public void attributeRemoved(ServletRequestAttributeEvent evt) {
		System.out.println("### attribute removed: " + evt.getName() + " = " + evt.getValue());
	}

	public void attributeReplaced(ServletRequestAttributeEvent evt) {
		System.out.println("### attribute replaced: " + evt.getName() + " = " + evt.getValue());
	}

}
