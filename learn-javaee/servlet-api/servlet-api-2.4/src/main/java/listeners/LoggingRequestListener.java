package listeners;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

public class LoggingRequestListener implements ServletRequestListener {

	public void requestDestroyed(ServletRequestEvent evt) {
		System.out.println("### request destroyed: " + uri(evt));
	}

	public void requestInitialized(ServletRequestEvent evt) {
		System.out.println("### request initialized: " + uri(evt));
	}
	
	private static String uri(ServletRequestEvent evt) {
		HttpServletRequest req = (HttpServletRequest) evt.getServletRequest();
		return req.getRequestURI();
	}

}
