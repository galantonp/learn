package servlets;

public class ThreadLocalDemo {

	private static String normal = "normal";
	private static ThreadLocal<String> tl = new ThreadLocal<String>() {
		
		protected String initialValue() {
			return "tl";
		};
	};
	
	
	private static class Demo implements Runnable {
		
		private String name;
		
		public Demo(String name) {
			this.name = name;
		}
		
		public void run() {
			
			System.out.println("normal = " + normal);
			System.out.println("tl = " + tl.get());
			
			normal = name;
			tl.set(name);
			
		};
	}
	
	
	public static void main(String[] args) throws Exception {
		
		Thread t1 = new Thread(new Demo("thread 1"));
		Thread t2 = new Thread(new Demo("thread 2"));
		
		
		t1.start();
		t1.join();
		t2.start();
		t2.join();
		
	}
	
}
