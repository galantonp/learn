package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IncludedServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		getServletContext().log("### included servlet executing");
		
		resp.setContentType("text/plain");
		
		PrintWriter out = resp.getWriter();
		out.println("request uri: " + req.getRequestURI());
		out.println("context path: " + req.getContextPath());
		out.println("servlet path: " + req.getServletPath());
		out.println("path info: " + req.getPathInfo());
		out.println("query string: " + req.getQueryString());
		out.println("javax.servlet.include.request_uri: " + req.getAttribute("javax.servlet.include.request_uri"));
		out.println("javax.servlet.include.context_path: " + req.getAttribute("javax.servlet.include.context_path"));
		out.println("javax.servlet.include.servlet_path: " + req.getAttribute("javax.servlet.include.servlet_path"));
		out.println("javax.servlet.include.path_info: " + req.getAttribute("javax.servlet.include.path_info"));
		out.println("javax.servlet.include.query_string: " + req.getAttribute("javax.servlet.include.query_string"));
	}
}
