package ro.galanton.learn.jpa;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ro.galanton.learn.jpa.matchers.TableMatcher;
import ro.galanton.learn.jpa.model.Child1;
import ro.galanton.learn.jpa.model.Child11;
import ro.galanton.learn.jpa.model.Child12;
import ro.galanton.learn.jpa.model.Child13;
import ro.galanton.learn.jpa.model.Child14a;
import ro.galanton.learn.jpa.model.Child14b;
import ro.galanton.learn.jpa.model.Child15;
import ro.galanton.learn.jpa.model.Child16;
import ro.galanton.learn.jpa.model.Child17a;
import ro.galanton.learn.jpa.model.Child17b;
import ro.galanton.learn.jpa.model.Child18a;
import ro.galanton.learn.jpa.model.Child18b;
import ro.galanton.learn.jpa.model.Child19a;
import ro.galanton.learn.jpa.model.Child19b;
import ro.galanton.learn.jpa.model.Child2;
import ro.galanton.learn.jpa.model.Child20a;
import ro.galanton.learn.jpa.model.Child20b;
import ro.galanton.learn.jpa.model.Child22a;
import ro.galanton.learn.jpa.model.Child22b;
import ro.galanton.learn.jpa.model.Child24a;
import ro.galanton.learn.jpa.model.Child24b;
import ro.galanton.learn.jpa.model.Child7a;
import ro.galanton.learn.jpa.model.Child7b;
import ro.galanton.learn.jpa.model.Child8;
import ro.galanton.learn.jpa.model.Child9a;
import ro.galanton.learn.jpa.model.Child9b;
import ro.galanton.learn.jpa.model.Parent1;
import ro.galanton.learn.jpa.model.Parent10;
import ro.galanton.learn.jpa.model.Parent10Key;
import ro.galanton.learn.jpa.model.Parent11;
import ro.galanton.learn.jpa.model.Parent12;
import ro.galanton.learn.jpa.model.Parent13;
import ro.galanton.learn.jpa.model.Parent14;
import ro.galanton.learn.jpa.model.Parent15;
import ro.galanton.learn.jpa.model.Parent16;
import ro.galanton.learn.jpa.model.Parent17;
import ro.galanton.learn.jpa.model.Parent18;
import ro.galanton.learn.jpa.model.Parent19;
import ro.galanton.learn.jpa.model.Parent2;
import ro.galanton.learn.jpa.model.Parent20;
import ro.galanton.learn.jpa.model.Parent22;
import ro.galanton.learn.jpa.model.Parent24;
import ro.galanton.learn.jpa.model.Parent3;
import ro.galanton.learn.jpa.model.Parent4;
import ro.galanton.learn.jpa.model.Parent5;
import ro.galanton.learn.jpa.model.Parent6;
import ro.galanton.learn.jpa.model.Parent7;
import ro.galanton.learn.jpa.model.Parent8;
import ro.galanton.learn.jpa.model.Parent8Key;
import ro.galanton.learn.jpa.model.Parent9;

public class EmbeddableTest extends JpaTestBase {

	private Connection con;
	private DatabaseMetaData metadata;
	
	@Before
	public void openConnection() throws Exception {
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mentoring", "petru", "123");
		metadata = con.getMetaData();
	}
	
	@After
	public void closeConnection() throws Exception {
		if (con != null) {
			con.close();
		}
	}
	
	@Test
	public void entity_can_have_embedded_class() {
		// setup
		Parent1 parent = new Parent1(1L, "parent", new Child1("child"));
		em.persist(parent);
		em.flush();
		
		// test
		Parent1 actual = em.find(Parent1.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent1")
				.withColumn("id")
				.andColumn("parentName")
				.andColumn("childName")
				.andPrimaryKey("id"));
		
		assertNotNull(actual);
		assertNotNull(actual.getChild());
		assertEquals("child", actual.getChild().getChildName());
	}
	
	@Test
	public void entity_can_have_collection_of_embedded_class() {
		// setup
		Parent2 parent = new Parent2(1L, "parent", Arrays.asList(new Child2("child1"), new Child2("child2")));
		em.persist(parent);
		em.flush();
		
		// test
		Parent2 actual = em.find(Parent2.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent2_children")
				.withColumn("Parent2_id")
					.withForeignKeyTo("Parent2", "id")
				.andColumn("childName")
				.andNoPrimaryKey());
		
		assertNotNull(actual);
		assertNotNull(actual.getChildren());
		assertEquals(2, actual.getChildren().size());

		List<String> childNames = actual.getChildren().stream()
				.map(Child2::getChildName)
				.collect(Collectors.toList());
		
		assertTrue(childNames.contains("child1"));
		assertTrue(childNames.contains("child2"));
	}
	
	@Test
	public void entity_with_composite_key_can_have_collection_of_embedded_class() {
		// setup
		Child8 child = new Child8("child");
		Parent8 parent = new Parent8(1, 2, "parent", Arrays.asList(child));
		em.persist(parent);
		em.flush();
		
		// test
		Parent8 actual = em.find(Parent8.class, new Parent8Key(1, 2));
		
		// verify
		assertThat(metadata, hasTable("Parent8_children")
				.withColumn("Parent8_id1")
					.withForeignKeyTo("Parent8", "id1")
				.andColumn("Parent8_id2")
					.withForeignKeyTo("Parent8", "id2")
				.andColumn("childName")
				.andNoPrimaryKey());
		
		assertNotNull(actual);
		assertEquals("parent", actual.getName());
		assertThat(
				actual.getChildren().stream()
					.map(Child8::getChildName)
					.collect(Collectors.toList()),
				contains("child"));
	}
	
	@Test
	public void entity_can_have_collection_of_base_type() {
		// setup
		Parent3 parent = new Parent3(1L, "parent", Arrays.asList("child1", "child2"));
		em.persist(parent);
		em.flush();
		
		// test
		Parent3 actual = em.find(Parent3.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent3_childNames")
				.withColumn("Parent3_id")
					.withForeignKeyTo("Parent3", "id")
				.andColumn("childNames")
				.andNoPrimaryKey());
		
		assertNotNull(actual);
		assertEquals(2, actual.getChildNames().size());
		assertTrue(actual.getChildNames().contains("child1"));
		assertTrue(actual.getChildNames().contains("child2"));
	}
	
	@Test
	public void entity_with_composite_key_can_have_collection_of_base_type() {
		// setup
		Parent10 parent = new Parent10(1, 2, "parent", Arrays.asList("child1", "child2"));
		em.persist(parent);
		em.flush();
		
		// test
		Parent10 actual = em.find(Parent10.class, new Parent10Key(1, 2));
		
		// verify
		assertThat(metadata, hasTable("Parent10_children")
				.withColumn("Parent10_id1")
					.withForeignKeyTo("Parent10", "id1")
				.andColumn("Parent10_id2")
					.withForeignKeyTo("Parent10", "id2")
				.andColumn("children")
				.andNoPrimaryKey());
		
		assertNotNull(actual);
		assertThat(actual.getChildren(), contains("child1", "child2"));
	}
	
	@Test
	public void table_for_collection_of_embeddable_class_can_be_customized() {
		// setup
		Child11 child1 = new Child11("child1");
		Child11 child2 = new Child11("child2");
		Parent11 parent = new Parent11(1, "parent", Arrays.asList(child1, child2));
		em.persist(parent);
		em.flush();
		
		// test
		Parent11 actual = em.find(Parent11.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("parent11_children")
				.withColumn("parent11_id")
					.withForeignKeyTo("Parent11", "id")
				.andColumn("child_name")
				.andNoPrimaryKey());
		
		assertNotNull(actual);
		assertThat(
				parent.getChildren().stream().map(Child11::getName).collect(Collectors.toList()),
				contains("child1", "child2"));
	}
	
	@Test
	public void table_for_collection_of_base_type_can_be_customized() {
		// setup
		Parent4 parent = new Parent4(1L, "parent", Arrays.asList("child1", "child2"));
		em.persist(parent);
		em.flush();
		
		// test
		Parent4 actual = em.find(Parent4.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("parent4_children")
				.withColumn("parentId")
					.withForeignKeyTo("Parent4", "id")
				.andColumn("childName")
				.andNoPrimaryKey());
		
		assertNotNull(actual);
		assertEquals(2, actual.getChildNames().size());
		assertTrue(actual.getChildNames().contains("child1"));
		assertTrue(actual.getChildNames().contains("child2"));
	}
	
	@Test
	public void entity_can_have_map_with_base_type_key_and_value() {
		// setup
		Parent5 parent = new Parent5(1, "parent");
		parent.getChildren().put("child1-key", "child1-value");
		parent.getChildren().put("child2-key", "child2-value");
		em.persist(parent);
		em.flush();
		
		// test
		Parent5 actual = em.find(Parent5.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent5_children")
				.withColumn("Parent5_id")
					.withForeignKeyTo("Parent5", "id")
				.andColumn("children_KEY")
				.andColumn("children")
				.andPrimaryKey("Parent5_id", "children_KEY"));
		
		assertNotNull(actual);
		assertThat(actual.getChildren(), hasEntry("child1-key", "child1-value"));
		assertThat(actual.getChildren(), hasEntry("child2-key", "child2-value"));
	}
	
	@Test
	public void entity_can_have_map_with_base_type_key_and_embeddable_value() {
		// setup
		Parent15 parent = new Parent15(1, "parent");
		parent.getChildren().put("child1-key", new Child15("child1-value"));
		parent.getChildren().put("child2-key", new Child15("child2-value"));
		em.persist(parent);
		em.flush();
		em.clear();
		
		// test
		Parent15 actual = em.find(Parent15.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent15_children")
				.withColumn("Parent15_id")
					.withForeignKeyTo("Parent15", "id")
				.andColumn("children_KEY")
				.andColumn("childName")
				.andPrimaryKey("Parent15_id", "children_KEY"));
		
		assertNotNull(actual);
		
		assertThat(actual.getChildren(), hasKey("child1-key"));
		assertEquals("child1-value", actual.getChildren().get("child1-key").getChildName());
		
		assertThat(actual.getChildren(), hasKey("child2-key"));
		assertEquals("child2-value", actual.getChildren().get("child2-key").getChildName());
	}
	
	@Test
	public void entity_can_have_map_with_embeddable_key_and_base_type_value() {
		// setup
		Child16 child1 = new Child16("child1-key");
		Child16 child2 = new Child16("child2-key");
		Parent16 parent = new Parent16(1, "parent");
		parent.getChildren().put(child1, "child1-value");
		parent.getChildren().put(child2, "child2-value");
		em.persist(parent);
		em.flush();
		em.clear();
		
		// test
		Parent16 actual = em.find(Parent16.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent16_children")
				.withColumn("Parent16_id")
					.withForeignKeyTo("Parent16", "id")
				.andColumn("childName") // column name = field name of Child16.childName
				.andColumn("children") // column name = field name of Parent16.children
				.andPrimaryKey("Parent16_id", "childName")); 
		
		assertNotNull(actual);
		
		assertThat(actual.getChildren(), hasEntry(child1, "child1-value"));
		assertThat(actual.getChildren(), hasEntry(child2, "child2-value"));
	}
	
	@Test
	public void entity_can_have_map_with_embeddable_key_and_value() {
		// setup
		Child17a child1Key = new Child17a("child1-key");
		Child17b child1Value = new Child17b("child1-value");
		Child17a child2Key = new Child17a("child2-key");
		Child17b child2Value = new Child17b("child2-value");
		Parent17 parent = new Parent17(1, "parent");
		parent.getChildren().put(child1Key, child1Value);
		parent.getChildren().put(child2Key, child2Value);
		em.persist(parent);
		em.flush();
		em.clear();
		
		// test
		Parent17 actual = em.find(Parent17.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent17_children")
				.withColumn("Parent17_id")
					.withForeignKeyTo("Parent17", "id")
				.andColumn("child_key")
				.andColumn("child_value")
				.andPrimaryKey("Parent17_id", "child_key"));
		
		assertNotNull(actual);
		
		assertThat(actual.getChildren(), hasKey(child1Key));
		assertEquals("child1-value", actual.getChildren().get(child1Key).getName());
		
		assertThat(actual.getChildren(), hasKey(child2Key));
		assertEquals("child2-value", actual.getChildren().get(child2Key).getName());
	}
	
	// TODO can entity have element collection of type Map<Basic, Collection<Basic>> ?
	
	@Test
	public void table_for_map_of_base_type_key_and_value_can_be_customized() {
		// setup
		Parent6 parent = new Parent6(1, "parent");
		parent.getChildren().put("child1-key", "child1-value");
		parent.getChildren().put("child2-key", "child2-value");
		em.persist(parent);
		em.flush();
		
		// test
		Parent6 actual = em.find(Parent6.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("parent6_children")
				.withColumn("parentId")
					.withForeignKeyTo("Parent6", "id")
				.andColumn("childKey")
				.andColumn("childValue")
				.andPrimaryKey("parentId", "childKey"));
		
		assertNotNull(actual);
		assertThat(actual.getChildren(), hasEntry("child1-key", "child1-value"));
		assertThat(actual.getChildren(), hasEntry("child2-key", "child2-value"));
	}
	
	@Test
	public void table_for_map_of_base_type_key_and_embeddable_value_can_be_customized() {
		// setup
		Parent12 parent = new Parent12(1, "parent");
		parent.getChildren().put("child1-key", new Child12("child1"));
		parent.getChildren().put("child2-key", new Child12("child2"));
		em.persist(parent);
		em.flush();
		
		// test
		Parent12 actual = em.find(Parent12.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("parent12_children")
				.withColumn("parent12_id")
					.withForeignKeyTo("Parent12", "id")
				.andColumn("child_key")
				.andColumn("child_name")
				.andPrimaryKey("parent12_id", "child_key"));
		
		assertNotNull(actual);
		
		assertThat(parent.getChildren(), hasKey("child1-key"));
		assertEquals("child1", parent.getChildren().get("child1-key").getName());
		
		assertThat(parent.getChildren(), hasKey("child2-key"));
		assertEquals("child2", parent.getChildren().get("child2-key").getName());
	}
	
	@Test
	public void table_for_map_of_embeddable_key_and_base_type_value_can_be_customized() {
		// setup
		Child13 child1 = new Child13("child1");
		Child13 child2 = new Child13("child2");
		Parent13 parent = new Parent13(1, "parent");
		parent.getChildren().put(child1, "child1-value");
		parent.getChildren().put(child2, "child2-value");
		em.persist(parent);
		em.flush();
		em.clear();
		
		// test
		Parent13 actual = em.find(Parent13.class, 1L);
		
		assertThat(metadata, hasTable("parent13_children")
				.withColumn("parent13_id")
					.withForeignKeyTo("Parent13", "id")
				.andColumn("child_key")
				.andColumn("child_value")
				.andPrimaryKey("parent13_id", "child_key"));
		
		assertNotNull(actual);
		assertThat(parent.getChildren(), hasEntry(child1, "child1-value"));
		assertThat(parent.getChildren(), hasEntry(child2, "child2-value"));
	}
	
	@Test
	public void table_for_map_of_embeddable_key_and_value_can_be_customized() {
		// setup
		Child14a child1Key = new Child14a("child1-key");
		Child14b child1Value = new Child14b("child1-value");
		Child14a child2Key = new Child14a("child2-key");
		Child14b child2Value = new Child14b("child2-value");
		Parent14 parent = new Parent14(1, "parent");
		parent.getChildren().put(child1Key, child1Value);
		parent.getChildren().put(child2Key, child2Value);
		em.persist(parent);
		em.flush();
		em.clear();
		
		// test
		Parent14 actual = em.find(Parent14.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("parent14_children")
			.withColumn("parent_id")
				.withForeignKeyTo("Parent14", "id")
			.andColumn("child_key")
			.andColumn("child_value")
			.andPrimaryKey("parent_id", "child_key"));
		
		assertNotNull(actual);
		
		assertThat(actual.getChildren(), hasKey(child1Key));
		assertEquals("child1-value", actual.getChildren().get(child1Key).getName());
		
		assertThat(actual.getChildren(), hasKey(child2Key));
		assertEquals("child2-value", actual.getChildren().get(child2Key).getName());
	}
	
	@Test
	public void embeddable_class_may_contain_another_embeddable_class() {
		// setup
		Child7b child7b = new Child7b("child7b");
		Child7a child7a = new Child7a("child7a", child7b);
		Parent7 parent = new Parent7(1, "parent", child7a);
		em.persist(parent);
		em.flush();
		
		// test
		Parent7 actual = em.find(Parent7.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent7")
			.withColumn("id")
			.andColumn("parentName")
			.andColumn("child7aName")
			.andColumn("child7bName")
			.andPrimaryKey("id"));
		
		assertNotNull(actual);
		assertEquals("parent", actual.getName());
		assertEquals("child7a", actual.getChild().getName());
		assertEquals("child7b", actual.getChild().getChild().getName());
	}

	@Test
	public void circular_containment_among_embeddable_classes_is_not_permitted() {
		// neither direct or indirect circular containment is permitted
		
		// not demonstrated due to breaking the entire persistence unit and 
		// causing Eclipse to crash with StackOverflowError when JPA validation
		// is enabled.
	}
	
	@Test
	public void embeddable_class_can_have_one_to_one_relationship_to_entity() {
		// setup
		Child9b child9b = new Child9b(1, "child9b");
		Child9a child9a = new Child9a("child9a", child9b);
		Parent9 parent = new Parent9(1, "parent", child9a);
		em.persist(child9b);
		em.persist(parent);
		em.flush();
		
		// test
		Parent9 actual = em.find(Parent9.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent9")
				.withColumn("id")
				.andColumn("parent_name")
				.andColumn("child9a_name")
				.andColumn("child9b_id")
					.withForeignKeyTo("Child9b", "id")
				.andPrimaryKey("id"));
		
		assertNotNull(actual);
		assertEquals("child9a", actual.getChild().getName());
		assertEquals("child9b", actual.getChild().getChild().getName());
	}
	
	@Test
	public void embeddable_class_can_have_one_to_many_relationship_to_entity() {
		// setup
		Child18b child18b1 = new Child18b(1, "child18b1");
		Child18b child18b2 = new Child18b(2, "child18b2");
		Child18a child18a = new Child18a("child18a", Arrays.asList(child18b1, child18b2));
		Parent18 parent = new Parent18(1, "parent", child18a);
		em.persist(child18b1);
		em.persist(child18b2);
		em.persist(parent);
		em.flush();
		em.clear();
		
		// test
		Parent18 actual = em.find(Parent18.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent18")
				.withColumn("id")
				.andColumn("parentName")
				.andColumn("childName")
				.andPrimaryKey("id"));
		
		assertThat(metadata, hasTable("Child18b")
				.withColumn("id")
				.andColumn("name")
				.andPrimaryKey("id"));
		
		assertThat(metadata, hasTable("Parent18_Child18b")
				.withColumn("Parent18_id")
					.withForeignKeyTo("Parent18", "id")
				.andColumn("children_id")
					.withForeignKeyTo("Child18b", "id")
				.andNoPrimaryKey());
		// TODO example on how to customize the join table
		
		assertNotNull(actual);
		assertNotNull(actual.getChild());
		assertEquals("child18a", actual.getChild().getName());
		assertThat(
				actual.getChild().getChildren().stream().map(Child18b::getName).collect(Collectors.toList()),
				contains("child18b1", "child18b2"));
	}
	
	@Test
	public void embeddable_class_can_have_many_to_one_relationship_to_entity() {
		// setup
		Child19b child19b = new Child19b(1, "child19b");
		
		Child19a child19a1 = new Child19a("child19a1", child19b);
		Parent19 parent1 = new Parent19(1, "parent1", child19a1);
		
		Child19a child19a2 = new Child19a("child19a2", child19b);
		Parent19 parent2 = new Parent19(2, "parent2", child19a2);
		
		em.persist(child19b);
		em.persist(parent1);
		em.persist(parent2);
		em.flush();
		em.clear();
		
		// test
		Parent19 actual1 = em.find(Parent19.class, 1L);
		Parent19 actual2 = em.find(Parent19.class, 2L);
		
		// verify
		assertThat(metadata, hasTable("Parent19")
				.withColumn("id")
				.andColumn("parentName")
				.andColumn("childName")
				.andColumn("child_id")
					.withForeignKeyTo("Child19b", "id")
				.andPrimaryKey("id"));
		// TODO example on how to customize the name of the fk column

		assertNotNull(actual1);
		assertEquals("parent1", actual1.getName());
		assertEquals("child19a1", actual1.getChild().getName());
		assertEquals(1L, actual1.getChild().getChild().getId());
		assertEquals("child19b", actual1.getChild().getChild().getName());
		
		assertNotNull(actual2);
		assertEquals("parent2", actual2.getName());
		assertEquals("child19a2", actual2.getChild().getName());
		assertEquals(1L, actual2.getChild().getChild().getId());
		assertEquals("child19b", actual2.getChild().getChild().getName());
	}
	
	@Test
	public void embeddable_class_can_have_many_to_many_relationship_to_entity() {
		// setup
		Child20b child20b1 = new Child20b(1L, "child20b1");
		Child20b child20b2 = new Child20b(2L, "child20b2");
		Child20b child20b3 = new Child20b(3L, "child20b3");
		
		Child20a child20a1 = new Child20a("child20a1", Arrays.asList(child20b1, child20b2));
		Child20a child20a2 = new Child20a("child20a2", Arrays.asList(child20b1, child20b3));
		
		Parent20 parent1 = new Parent20(1L, "parent1", child20a1);
		Parent20 parent2 = new Parent20(2L, "parent2", child20a2);
		
		em.persist(child20b1);
		em.persist(child20b2);
		em.persist(child20b3);
		em.persist(parent1);
		em.persist(parent2);
		em.flush();
		em.clear();
		
		// test
		Parent20 actual1 = em.find(Parent20.class, 1L);
		Parent20 actual2 = em.find(Parent20.class, 2L);
		
		// verify
		assertThat(metadata, hasTable("Parent20_Child20b")
				.withColumn("Parent20_id")
					.withForeignKeyTo("Parent20", "id")
				.andColumn("children_id")
					.withForeignKeyTo("Child20b", "id")
				.andNoPrimaryKey());
		// TODO example on how to customize the join table
		
		assertNotNull(actual1);
		assertEquals("parent1", actual1.getName());
		assertEquals("child20a1", actual1.getChild().getName());
		assertThat(
				actual1.getChild().getChildren().stream().map(Child20b::getName).collect(Collectors.toList()),
				contains("child20b1", "child20b2"));
		
		assertNotNull(actual2);
		assertEquals("parent2", actual2.getName());
		assertEquals("child20a2", actual2.getChild().getName());
		assertThat(
				actual2.getChild().getChildren().stream().map(Child20b::getName).collect(Collectors.toList()),
				contains("child20b1", "child20b3"));
	}
	
	@Test
	public void embeddable_in_element_collection_may_not_have_an_element_collection() {
		// uncomment @ElementCollection in Parent21
	}
	
	@Test
	public void embeddable_in_element_collection_may_have_one_to_one_relationship() {
		// setup
		Child22b child22b1 = new Child22b(1, "child22b1");
		Child22b child22b2 = new Child22b(2, "child22b2");
		Child22a child22a1 = new Child22a("child22a1", child22b1);
		Child22a child22a2 = new Child22a("child22a2", child22b2);
		Parent22 parent = new Parent22(1, "parent", Arrays.asList(child22a1, child22a2));
		em.persist(child22b1);
		em.persist(child22b2);
		em.persist(parent);
		em.flush();
		em.clear();
		
		// test
		Parent22 actual = em.find(Parent22.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent22_children")
				.withColumn("Parent22_id")
					.withForeignKeyTo("Parent22", "id")
				.andColumn("name")
				.andColumn("child_id")
					.withForeignKeyTo("Child22b", "id")
				.andNoPrimaryKey());
		
		assertNotNull(actual);
		
		Optional<Child22a> child1 = parent.getChildren().stream()
			.filter(child -> "child22a1".equals(child.getName()))
			.findFirst();
		assertTrue(child1.isPresent());
		assertNotNull(child1.get().getChild());
		assertEquals("child22b1", child1.get().getChild().getName());
		
		Optional<Child22a> child2 = parent.getChildren().stream()
			.filter(child -> "child22a2".equals(child.getName()))
			.findFirst();
		assertTrue(child2.isPresent());
		assertNotNull(child2.get().getChild());
		assertEquals("child22b2", child2.get().getChild().getName());
	}
	
	@Test
	public void embeddable_in_element_collection_may_not_have_one_to_many_relationship() {
		// uncomment @ElementCollection in Parent23
	}
	
	@Test
	public void embeddable_in_element_collection_may_have_many_to_one_relationship() {
		// setup
		Child24b child24b = new Child24b(1, "child24b");
		Child24a child24a1 = new Child24a("child24a1", child24b);
		Child24a child24a2 = new Child24a("child24a2", child24b);
		Parent24 parent = new Parent24(1, "parent", Arrays.asList(child24a1, child24a2));
		em.persist(child24b);
		em.persist(parent);
		em.flush();
		em.clear();
		
		// test
		Parent24 actual = em.find(Parent24.class, 1L);
		
		// verify
		assertThat(metadata, hasTable("Parent24_children")
				.withColumn("Parent24_id")
					.withForeignKeyTo("Parent24", "id")
				.andColumn("name")
				.andColumn("child_id")
					.withForeignKeyTo("Child24b", "id")
				.andNoPrimaryKey());
		
		assertNotNull(actual);

		Map<String, Child24a> children = parent.getChildren().stream()
			.collect(Collectors.toMap(Child24a::getName, child -> child));
		
		assertThat(children, hasKey("child24a1"));
		assertThat(children.get("child24a1").getChild(), is(not(nullValue())));
		assertThat(children.get("child24a1").getChild().getName(), is("child24b"));
		
		assertThat(children, hasKey("child24a2"));
		assertThat(children.get("child24a2").getChild(), is(not(nullValue())));
		assertThat(children.get("child24a2").getChild().getName(), is("child24b"));
		
	}
	
	@Test
	public void embeddable_in_element_collection_may_not_have_many_to_many_relationship() {
		// uncomment @ElementCollection in Parent25
	}
	
	private TableMatcher hasTable(String tableName) {
		return new TableMatcher(tableName);
	}
}
