package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@SuppressWarnings("unused")
public class Parent25 {

	@Id
	private long id;
	
	private String name;
	
	// uncomment @ElementCollection and comment @Transient => JPA error
//	@ElementCollection
	@Transient
	private List<Child25a> children;

	public Parent25() {
	}

	public Parent25(long id, String name, List<Child25a> children) {
		this.id = id;
		this.name = name;
		this.children = children;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Child25a> getChildren() {
		return children;
	}

	public void setChildren(List<Child25a> children) {
		this.children = children;
	}
	
}
