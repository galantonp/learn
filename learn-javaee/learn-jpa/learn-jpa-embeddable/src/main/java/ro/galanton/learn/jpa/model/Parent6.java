package ro.galanton.learn.jpa.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;

@Entity
public class Parent6 {

	@Id
	private long id;
	
	private String parentName;

	@Column(name = "childValue") // customize the name of the value column
	@MapKeyColumn(name = "childKey") // customize the name of the key column
	@CollectionTable(
		name = "parent6_children", // customize the name of the table
		joinColumns = @JoinColumn(
			name = "parentId", // customize the name of the FK column
			foreignKey = @ForeignKey(name = "FK_parent6")
		)
	) 
	@ElementCollection
	private Map<String, String> children = new HashMap<>();

	public Parent6() {
	}

	public Parent6(long id, String parentName) {
		this.id = id;
		this.parentName = parentName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Map<String, String> getChildren() {
		return children;
	}

	public void setChildren(Map<String, String> children) {
		this.children = children;
	}
	
}
