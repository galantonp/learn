package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(Parent8Key.class)
public class Parent8 {
	
	@Id
	private long id1;
	
	@Id
	private long id2;
	
	private String name;

	@ElementCollection
	private List<Child8> children;

	public Parent8() {
	}

	public Parent8(long id1, long id2, String name, List<Child8> children) {
		this.id1 = id1;
		this.id2 = id2;
		this.name = name;
		this.children = children;
	}

	public long getId1() {
		return id1;
	}

	public void setId1(long id1) {
		this.id1 = id1;
	}

	public long getId2() {
		return id2;
	}

	public void setId2(long id2) {
		this.id2 = id2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Child8> getChildren() {
		return children;
	}

	public void setChild(List<Child8> children) {
		this.children = children;
	}
	
}
