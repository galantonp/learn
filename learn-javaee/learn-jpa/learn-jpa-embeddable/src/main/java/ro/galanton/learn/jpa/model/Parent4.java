package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity
public class Parent4 {

	@Id
	private long id;
	
	private String parentName;

	@Column(name = "childName") // custom name for the value column
	@CollectionTable(
		name = "parent4_children", // custom table name
		joinColumns = @JoinColumn(
			name = "parentId", // custom column name for the FK column
			foreignKey = @ForeignKey(name = "fk_parent4_children") // custom FK name not working (HHH-8862, HHH-11180)
		)
	)
	@ElementCollection
	private List<String> childNames;

	public Parent4() {
	}

	public Parent4(long id, String parentName, List<String> childNames) {
		this.id = id;
		this.parentName = parentName;
		this.childNames = childNames;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public List<String> getChildNames() {
		return childNames;
	}

	public void setChildNames(List<String> childNames) {
		this.childNames = childNames;
	}
	
}
