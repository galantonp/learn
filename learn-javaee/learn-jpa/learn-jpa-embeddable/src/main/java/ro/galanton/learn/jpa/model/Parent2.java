package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent2 {

	@Id
	private long id;
	
	private String parentName;
	
	@ElementCollection
	private List<Child2> children;

	public Parent2() {
	}

	public Parent2(long id, String parentName, List<Child2> children) {
		this.id = id;
		this.parentName = parentName;
		this.children = children;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public List<Child2> getChildren() {
		return children;
	}

	public void setChildren(List<Child2> children) {
		this.children = children;
	}
	
}
