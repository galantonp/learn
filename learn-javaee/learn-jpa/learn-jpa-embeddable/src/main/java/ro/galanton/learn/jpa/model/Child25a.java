package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.Embeddable;
import javax.persistence.ManyToMany;

@Embeddable
public class Child25a {

	private String name;
	
	@ManyToMany
	private List<Child25b> children;

	public Child25a() {
	}

	public Child25a(String name, List<Child25b> children) {
		this.name = name;
		this.children = children;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Child25b> getChildren() {
		return children;
	}

	public void setChildren(List<Child25b> children) {
		this.children = children;
	}
	
}
