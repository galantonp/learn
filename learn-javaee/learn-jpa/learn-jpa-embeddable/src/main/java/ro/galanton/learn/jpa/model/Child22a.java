package ro.galanton.learn.jpa.model;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;

@Embeddable
public class Child22a {

	private String name;
	
	@OneToOne
	private Child22b child;

	public Child22a() {
	}

	public Child22a(String name, Child22b child) {
		this.name = name;
		this.child = child;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Child22b getChild() {
		return child;
	}

	public void setChild(Child22b child) {
		this.child = child;
	}
	
}
