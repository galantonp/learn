package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(Parent10Key.class)
public class Parent10 {
	
	@Id
	private long id1;
	
	@Id
	private long id2;
	
	private String name;
	
	@ElementCollection
	private List<String> children;

	public Parent10() {
	}

	public Parent10(long id1, long id2, String name, List<String> children) {
		this.id1 = id1;
		this.id2 = id2;
		this.name = name;
		this.children = children;
	}

	public long getId1() {
		return id1;
	}

	public void setId1(long id1) {
		this.id1 = id1;
	}

	public long getId2() {
		return id2;
	}

	public void setId2(long id2) {
		this.id2 = id2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getChildren() {
		return children;
	}

	public void setChildren(List<String> children) {
		this.children = children;
	}

}
