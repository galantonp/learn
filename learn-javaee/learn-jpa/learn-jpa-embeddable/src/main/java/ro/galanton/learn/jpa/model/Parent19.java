package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent19 {

	@Id
	private long id;
	
	@Column(name = "parentName")
	private String name;
	
	@Embedded
	private Child19a child;

	public Parent19() {
	}

	public Parent19(long id, String name, Child19a child) {
		super();
		this.id = id;
		this.name = name;
		this.child = child;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Child19a getChild() {
		return child;
	}

	public void setChild(Child19a child) {
		this.child = child;
	}
	
}
