package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
public class Child7a {

	@Column(name = "child7aName")
	private String name;
	
	@Embedded
	private Child7b child;

	public Child7a() {
		// TODO Auto-generated constructor stub
	}

	public Child7a(String name, Child7b child) {
		this.name = name;
		this.child = child;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Child7b getChild() {
		return child;
	}
	
	public void setChild(Child7b child) {
		this.child = child;
	}
	
}
