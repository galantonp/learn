package ro.galanton.learn.jpa.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent15 {

	@Id
	private long id;
	
	private String parentName;
	
	@ElementCollection
	private Map<String, Child15> children = new HashMap<>();

	public Parent15() {
	}

	public Parent15(long id, String parentName) {
		super();
		this.id = id;
		this.parentName = parentName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Map<String, Child15> getChildren() {
		return children;
	}

	public void setChildren(Map<String, Child15> children) {
		this.children = children;
	}
	
}
