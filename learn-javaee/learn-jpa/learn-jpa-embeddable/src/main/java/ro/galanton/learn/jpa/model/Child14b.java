package ro.galanton.learn.jpa.model;

import javax.persistence.Embeddable;

@Embeddable
public class Child14b {

	private String name;

	public Child14b() {
	}

	public Child14b(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
