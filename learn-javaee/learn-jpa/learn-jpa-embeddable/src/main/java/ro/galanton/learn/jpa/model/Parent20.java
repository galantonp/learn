package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent20 {

	@Id
	private long id;
	
	@Column(name = "parentName")
	private String name;

	@Embedded
	private Child20a child;
	
	public Parent20() {
	}

	public Parent20(long id, String name, Child20a child) {
		this.id = id;
		this.name = name;
		this.child = child;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Child20a getChild() {
		return child;
	}

	public void setChild(Child20a child) {
		this.child = child;
	}
	
}
