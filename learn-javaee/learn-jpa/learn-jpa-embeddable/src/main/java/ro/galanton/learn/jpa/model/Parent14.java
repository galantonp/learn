package ro.galanton.learn.jpa.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity
public class Parent14 {

	@Id
	private long id;
	
	private String name;
	
	@AttributeOverrides({
		@AttributeOverride(name = "key.name", column = @Column(name = "child_key")),
		@AttributeOverride(name = "value.name", column = @Column(name = "child_value"))
	})
	@ElementCollection
	@CollectionTable(
		name = "parent14_children",
		joinColumns = @JoinColumn(name = "parent_id")
	)
	private Map<Child14a, Child14b> children = new HashMap<>();

	public Parent14() {
	}

	public Parent14(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<Child14a, Child14b> getChildren() {
		return children;
	}

	public void setChildren(Map<Child14a, Child14b> children) {
		this.children = children;
	}
	
}
