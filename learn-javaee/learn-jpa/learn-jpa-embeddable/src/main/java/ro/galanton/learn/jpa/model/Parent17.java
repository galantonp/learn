package ro.galanton.learn.jpa.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent17 {

	@Id
	private long id;
	
	private String name;
	
	@ElementCollection
	private Map<Child17a, Child17b> children = new HashMap<>();

	public Parent17() {
	}

	public Parent17(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<Child17a, Child17b> getChildren() {
		return children;
	}

	public void setChildren(Map<Child17a, Child17b> children) {
		this.children = children;
	}
	
}
