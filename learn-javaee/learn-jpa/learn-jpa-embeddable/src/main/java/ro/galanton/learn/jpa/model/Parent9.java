package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent9 {

	@Id
	private long id;
	
	@Column(name = "parent_name")
	private String name;
	
	@Embedded
	private Child9a child;

	public Parent9() {
	}

	public Parent9(long id, String name, Child9a child) {
		this.id = id;
		this.name = name;
		this.child = child;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Child9a getChild() {
		return child;
	}

	public void setChild(Child9a child) {
		this.child = child;
	}
	
}
