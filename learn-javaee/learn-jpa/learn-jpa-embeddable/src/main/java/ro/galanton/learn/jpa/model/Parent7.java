package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent7 {

	@Id
	private long id;
	
	@Column(name = "parentName")
	private String name;

	@Embedded
	private Child7a child;

	public Parent7() {
	}

	public Parent7(long id, String name, Child7a child) {
		this.id = id;
		this.name = name;
		this.child = child;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Child7a getChild() {
		return child;
	}

	public void setChild(Child7a child) {
		this.child = child;
	}
	
}
