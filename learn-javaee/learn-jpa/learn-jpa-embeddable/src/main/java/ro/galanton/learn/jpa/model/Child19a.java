package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class Child19a {

	@Column(name = "childName")
	private String name;
	
	@ManyToOne
	private Child19b child;

	public Child19a() {
	}

	public Child19a(String name, Child19b child) {
		this.name = name;
		this.child = child;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Child19b getChild() {
		return child;
	}

	public void setChild(Child19b child) {
		this.child = child;
	}
	
}
