package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity
public class Parent11 {

	@Id
	private long id;
	
	private String name;
	
	@AttributeOverride(
		name = "name", // the name of the field in Child11 which is being overridden
		column = @Column(name = "child_name") // the column definition override
	)
	@ElementCollection
	@CollectionTable(
		name = "parent11_children",
		joinColumns = @JoinColumn(name = "parent11_id")
	)
	private List<Child11> children;

	public Parent11() {
	}

	public Parent11(long id, String name, List<Child11> children) {
		this.id = id;
		this.name = name;
		this.children = children;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Child11> getChildren() {
		return children;
	}

	public void setChildren(List<Child11> children) {
		this.children = children;
	}
	
}
