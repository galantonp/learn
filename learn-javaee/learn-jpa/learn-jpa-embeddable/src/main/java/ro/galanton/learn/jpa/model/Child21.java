package ro.galanton.learn.jpa.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;

@Embeddable
public class Child21 {

	@Column(name = "childName")
	private String name;
	
	@ElementCollection
	private List<String> aliases;

	public Child21() {
	}

	public Child21(String name, List<String> aliases) {
		super();
		this.name = name;
		this.aliases = aliases;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getAliases() {
		return aliases;
	}

	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}
	
}
