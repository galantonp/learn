package ro.galanton.learn.jpa.model;

import javax.persistence.Embeddable;

@Embeddable
public class Child16 {

	private String childName;

	public Child16() {
	}

	public Child16(String childName) {
		super();
		this.childName = childName;
	}

	public String getChildName() {
		return childName;
	}

	public void setChildName(String childName) {
		this.childName = childName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((childName == null) ? 0 : childName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Child16 other = (Child16) obj;
		if (childName == null) {
			if (other.childName != null)
				return false;
		} else if (!childName.equals(other.childName))
			return false;
		return true;
	}
	
}
