package ro.galanton.learn.jpa.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Parent5 {

	@Id
	private long id;
	
	private String parentName;
	
	@ElementCollection
	private Map<String, String> children = new HashMap<>();

	public Parent5() {
	}

	public Parent5(long id, String parentName) {
		this.id = id;
		this.parentName = parentName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Map<String, String> getChildren() {
		return children;
	}

	public void setChildren(Map<String, String> children) {
		this.children = children;
	}
	
}
