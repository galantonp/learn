package ro.galanton.learn.jpa.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

// implements hashCode() and equals(), as this embeddable is used as a map key
@Embeddable
public class Child13 {

	@Column(name = "child_key")
	private String name;

	public Child13() {
	}

	public Child13(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Child13 other = (Child13) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
