package ro.galanton.learn.jpa;

import java.lang.reflect.Field;

import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import ro.galanton.learn.jpa.annotation.ExcludePersistenceUnits;
import ro.galanton.learn.jpa.annotation.PersistenceUnitName;

public class ExcludePersistenceUnitRule implements MethodRule {

	@Override
	public Statement apply(Statement base, FrameworkMethod method, Object target) {
		return new ExcludePersistenceUnitStatement(base, method, target);
	}

	private static class ExcludePersistenceUnitStatement extends Statement {
		
		private Statement base;
		private FrameworkMethod method;
		private Object target;
		
		public ExcludePersistenceUnitStatement(Statement base, FrameworkMethod method, Object target) {
			this.base = base;
			this.method = method;
			this.target = target;
		}

		@Override
		public void evaluate() throws Throwable {
			String currentUnit = determineCurrentPersistenceUnit();
			if (isUnitExcluded(currentUnit)) {
				return;
			}
			
			try {
				base.evaluate();
			} catch (AssertionError e) {
				throw e;
			} catch (Throwable e) {
				throw new RuntimeException("Error in persistence unit " + currentUnit, e);
			}
		}
		
		private String determineCurrentPersistenceUnit() throws IllegalArgumentException, IllegalAccessException {
			for (Field field : target.getClass().getDeclaredFields()) {
				if (field.isAnnotationPresent(PersistenceUnitName.class)) {
					return (String) field.get(target);
				}
			}
			
			return null;
		}
		
		private boolean isUnitExcluded(String currentUnit) {
			ExcludePersistenceUnits excluded = method.getAnnotation(ExcludePersistenceUnits.class);
			if (excluded != null && currentUnit != null) {
				String[] units = excluded.value();
				for (String unit : units) {
					if (unit.equals(currentUnit)) {
						return true;
					}
				}
			}
			
			return false;
		}
	}
}
	