package ro.galanton.learn.jpa;

import java.lang.reflect.Field;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import ro.galanton.learn.jpa.annotation.PersistenceUnitName;
import ro.galanton.learn.jpa.annotation.TestsPersistenceUnits;

public class EntityManagerFactoryRule implements TestRule {

	@Override
	public Statement apply(Statement base, Description description) {
		return new EntityManagerFactoryStatement(base, description);
	}
	
	private static class EntityManagerFactoryStatement extends Statement {

		private Statement base;
		private Description description;
		
		public EntityManagerFactoryStatement(Statement base, Description description) {
			this.base = base;
			this.description = description;
		}
		
		@Override
		public void evaluate() throws Throwable {
			String[] persistenceUnits = determinePersistenceUnits();
			for (String persistenceUnit : persistenceUnits) {
				EntityManagerFactory emf = createEntityManagerFactory(persistenceUnit);
				try {
					injectEntityManagerFactory(emf, persistenceUnit);
					base.evaluate();
				} finally {
					closeEntityManagerFactory(emf);
				}
			}
		}

		private String[] determinePersistenceUnits() {
			TestsPersistenceUnits annotation = description.getAnnotation(TestsPersistenceUnits.class);
			return annotation.value();
		}
		
		private EntityManagerFactory createEntityManagerFactory(String persistenceUnit) {
			System.out.println("#### START: " + persistenceUnit + " ####");
			return Persistence.createEntityManagerFactory(persistenceUnit);
		}
		
		private void injectEntityManagerFactory(EntityManagerFactory emf, String persistenceUnit) throws IllegalArgumentException, IllegalAccessException {
			for (Field field : description.getTestClass().getDeclaredFields()) {
				if (field.isAnnotationPresent(PersistenceUnit.class)) {
					field.set(description.getTestClass(), emf);
				} else if (field.isAnnotationPresent(PersistenceUnitName.class)) {
					field.set(description.getTestClass(), persistenceUnit);
				}
			}
		}
		 
		private void closeEntityManagerFactory(EntityManagerFactory emf) {
			emf.close();
		}
	}

}
