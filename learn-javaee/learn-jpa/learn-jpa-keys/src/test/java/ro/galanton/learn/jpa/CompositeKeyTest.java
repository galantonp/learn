package ro.galanton.learn.jpa;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import ro.galanton.learn.jpa.annotation.PersistenceUnitName;
import ro.galanton.learn.jpa.annotation.TestsPersistenceUnits;
import ro.galanton.learn.jpa.model.composite.IdClassKey;
import ro.galanton.learn.jpa.model.composite.Parent1;
import ro.galanton.learn.jpa.model.composite.Parent2;
import ro.galanton.learn.jpa.model.composite.Parent2Key;
import ro.galanton.learn.jpa.model.composite.Parent3;
import ro.galanton.learn.jpa.model.composite.Parent3Key;
import ro.galanton.learn.jpa.model.composite.Parent4;
import ro.galanton.learn.jpa.model.composite.Parent5;
import ro.galanton.learn.jpa.model.composite.Parent5Key;
import ro.galanton.learn.jpa.model.composite.Parent6;
import ro.galanton.learn.jpa.model.composite.Parent6Key;
import ro.galanton.learn.jpa.model.composite.Derived1a;
import ro.galanton.learn.jpa.model.composite.Derived1b;
import ro.galanton.learn.jpa.model.composite.Derived2a;
import ro.galanton.learn.jpa.model.composite.Derived2b;
import ro.galanton.learn.jpa.model.composite.Derived3a;
import ro.galanton.learn.jpa.model.composite.Derived3b;
import ro.galanton.learn.jpa.model.composite.Derived4a;
import ro.galanton.learn.jpa.model.composite.Derived4aKey;
import ro.galanton.learn.jpa.model.composite.Derived4b;
import ro.galanton.learn.jpa.model.composite.Derived4bKey;
import ro.galanton.learn.jpa.model.composite.Derived5a;
import ro.galanton.learn.jpa.model.composite.Derived5aKey;
import ro.galanton.learn.jpa.model.composite.Derived5b;
import ro.galanton.learn.jpa.model.composite.Derived5bKey;
import ro.galanton.learn.jpa.model.composite.Derived6a;
import ro.galanton.learn.jpa.model.composite.Derived6aKey;
import ro.galanton.learn.jpa.model.composite.Derived6b;
import ro.galanton.learn.jpa.model.composite.Derived6bKey;
import ro.galanton.learn.jpa.model.composite.EmbeddableCompositeKey;
import ro.galanton.learn.jpa.model.composite.EmbeddedIdEntity;
import ro.galanton.learn.jpa.model.composite.IdClassEntity;

@TestsPersistenceUnits({"mssql", "oracle", "mysql"})
public class CompositeKeyTest {

	@ClassRule
	public static EntityManagerFactoryRule entityManagerFactoryRule = new EntityManagerFactoryRule();
	
	@Rule
	public ExcludePersistenceUnitRule testExclusionRule = new ExcludePersistenceUnitRule();
	
	@PersistenceUnit
	public static EntityManagerFactory emf;
	
	@PersistenceUnitName
	public static String persistenceUnit;
	
	private EntityManager em;
	private EntityTransaction tx;
	
	@Before
	public void setUp() {
		// create an entity manager for the current test
		em = emf.createEntityManager();
		
		// start a transaction for the current test
		tx = em.getTransaction();
		tx.begin();
	}
	
	@After
	public void tearDown() {
		tx.rollback();
		em.close();
	}

	@Test
	public void testIdClassCompositeKey() {
		IdClassEntity entity = new IdClassEntity();
		entity.setId1(1);
		entity.setId2(2);
		entity.setName("IdClass");
		em.persist(entity);
		em.flush();
		
		IdClassEntity actual = em.find(IdClassEntity.class, new IdClassKey(1, 2));
		
		assertNotNull(actual);
		assertEquals(entity.getName(), actual.getName());
	}
	
	@Test
	public void testEmbeddedIdCompositeKey() {
		EmbeddedIdEntity entity = new EmbeddedIdEntity();
		entity.setPk(new EmbeddableCompositeKey(1, 2));
		entity.setName("EmbeddedId");
		em.persist(entity);
		em.flush();
		
		EmbeddedIdEntity actual = em.find(EmbeddedIdEntity.class, new EmbeddableCompositeKey(1, 2));
		
		assertNotNull(actual);
		assertEquals(entity.getName(), actual.getName());
	}
	
	@Test
	public void testDerived1a() {
		Parent1 parent = new Parent1();
		parent.setName("parent");
		Derived1a derived = new Derived1a();
		derived.setName("derived");
		derived.setParent(parent);
		em.persist(parent);
		em.persist(derived);
		em.flush();
		
		Derived1a actual = em.find(Derived1a.class, parent.getId());
		
		assertNotNull(actual);
		assertEquals("derived", actual.getName());
		assertNotNull(actual.getParent());
		assertEquals("parent", actual.getParent().getName());
	}
	
	@Test
	public void testDerived1b() {
		Parent1 parent = new Parent1();
		parent.setName("parent");
		Derived1b derived = new Derived1b();
		derived.setName("derived");
		derived.setParent(parent);
		em.persist(parent);
		em.persist(derived);
		em.flush();
		
		Derived1b actual = em.find(Derived1b.class, derived.getId());
		
		assertNotNull(actual);
		assertEquals("derived", actual.getName());
		assertNotNull(actual.getParent());
		assertEquals("parent", actual.getParent().getName());
		assertEquals(parent.getId(), derived.getId());
	}
	
	@Test
	public void testDerived2a() {
		Parent2 parent = new Parent2();
		parent.setId1(1);
		parent.setId2(2);
		parent.setName("parent");
		em.persist(parent);
		
		Derived2a derived = new Derived2a();
		derived.setName("derived");
		derived.setParent(parent);
		em.persist(derived);
		
		// test
		em.flush();
		Derived2a actual = em.find(Derived2a.class, new Parent2Key(1, 2));
		
		// verify
		assertNotNull(actual);
		assertEquals("derived", actual.getName());
		assertEquals(parent, actual.getParent());
	}
	
	@Test
	public void testDerived2b() {
		Parent2 parent = new Parent2();
		parent.setId1(1);
		parent.setId2(2);
		parent.setName("parent");
		em.persist(parent);
		
		Derived2b derived = new Derived2b();
		derived.setName("derived");
		derived.setParent(parent);
		em.persist(derived);
		
		// test
		em.flush();
		Derived2b actual = em.find(Derived2b.class, new Parent2Key(1, 2));
		
		// verify
		assertNotNull(actual);
		assertEquals("derived", actual.getName());
		assertEquals(parent, actual.getParent());
	}
	
	@Test
	public void testDerived3a() {
		Parent3 parent = new Parent3();
		parent.setName("parent");
		parent.setPk(new Parent3Key(1, 2));
		em.persist(parent);
		
		Derived3a derived = new Derived3a();
		derived.setName("derived");
		derived.setParent(parent);
		em.persist(derived);
		em.flush();
		
		// test
		Derived3a actual = em.find(Derived3a.class, new Parent3Key(1, 2));
		
		// verify
		assertNotNull(actual);
		assertEquals("derived", actual.getName());
		assertEquals(parent, actual.getParent());
	}
	
	@Test
	public void testDerived3b() {
		Parent3 parent = new Parent3();
		parent.setName("parent");
		parent.setPk(new Parent3Key(1, 2));
		em.persist(parent);
		
		Derived3b derived = new Derived3b();
		derived.setName("derived");
		derived.setParent(parent);
		em.persist(derived);
		em.flush();
		
		// test
		Derived3b actual = em.find(Derived3b.class, new Parent3Key(1, 2));
		
		// verify
		assertNotNull(actual);
		assertEquals("derived", actual.getName());
		assertEquals(parent, actual.getParent());
	}
	
	@Test
	public void testDerived4a() {
		// setup
		Parent4 parent = new Parent4(1, "parent");
		Derived4a derived = new Derived4a(parent, 2, "derived");
		em.persist(parent);
		em.persist(derived);
		em.flush();
		
		// test
		Derived4a actual = em.find(Derived4a.class, new Derived4aKey(1, 2));
		
		// verify
		assertNotNull(actual);
		assertEquals(1, actual.getParent().getId());
		assertEquals("parent", actual.getParent().getName());
		assertEquals(2, actual.getDerivedId());
		assertEquals("derived", actual.getName());
	}
	
	@Test
	public void testDerived4b() {
		// setup
		Parent4 parent = new Parent4(1, "parent");
		Derived4b derived = new Derived4b(new Derived4bKey(1, 2), parent, "derived");
		em.persist(parent);
		em.persist(derived);
		
		// test
		Derived4b actual = em.find(Derived4b.class, new Derived4bKey(1, 2));
		
		// verify
		assertNotNull(actual);
		assertEquals(1, actual.getParent().getId());
		assertEquals("parent", actual.getParent().getName());
		assertEquals(1, actual.getPk().getParentId());
		assertEquals(2, actual.getPk().getDerivedId());
		assertEquals("derived", actual.getName());
	}
	
	@Test
	public void testDerived5a() {
		// setup
		Parent5 parent = new Parent5(1, 2, "parent");
		Derived5a derived = new Derived5a(parent, 3, "derived");
		em.persist(parent);
		em.persist(derived);
		em.flush();
		
		// test
		Derived5a actual = em.find(Derived5a.class, new Derived5aKey(new Parent5Key(1, 2), 3));
		
		// verify
		assertNotNull(actual);
		assertEquals(1, actual.getParent().getParentId1());
		assertEquals(2, actual.getParent().getParentId2());
		assertEquals("parent", actual.getParent().getName());
		assertEquals(3, actual.getDerivedId());
		assertEquals("derived", actual.getName());
	}
	
	@Test
	public void testDerived5b() {
		// setup
		Parent5 parent = new Parent5(1, 2, "parent");
		Derived5b derived = new Derived5b(new Derived5bKey(new Parent5Key(1, 2), 3), parent, "derived");
		em.persist(parent);
		em.persist(derived);
		em.flush();
		
		// test
		Derived5b actual = em.find(Derived5b.class, derived.getPk());
		
		// verify
		assertNotNull(actual);
		assertEquals(1, actual.getPk().getParentId().getParentId1());
		assertEquals(2, actual.getPk().getParentId().getParentId2());
		assertEquals(3, actual.getPk().getDerivedId());
		assertEquals(1, actual.getParent().getParentId1());
		assertEquals(2, actual.getParent().getParentId2());
		assertEquals("parent", actual.getParent().getName());
		assertEquals("derived", actual.getName());
	}
	
	@Test
	public void testDerived6a() {
		// setup
		Parent6 parent = new Parent6(new Parent6Key(1, 2), "parent");
		Derived6a derived = new Derived6a(parent, 3, "derived");
		em.persist(parent);
		em.persist(derived);
		em.flush();
		
		// test
		Derived6a actual = em.find(Derived6a.class, new Derived6aKey(new Parent6Key(1, 2), 3));
		
		// verify
		assertNotNull(actual);
		assertEquals(1, actual.getParent().getPk().getParentId1());
		assertEquals(2, actual.getParent().getPk().getParentId2());
		assertEquals("parent", actual.getParent().getName());
		assertEquals(3, actual.getDerivedId());
		assertEquals("derived", actual.getName());
	}
	
	@Test
	public void testDerived6b() {
		// setup
		Parent6 parent = new Parent6(new Parent6Key(1, 2), "parent");
		Derived6b derived = new Derived6b(new Derived6bKey(parent.getPk(), 3), parent, "derived");
		em.persist(parent);
		em.persist(derived);
		em.flush();
		
		// test
		Derived6b actual = em.find(Derived6b.class, new Derived6bKey(new Parent6Key(1, 2), 3));
		
		// verify
		assertNotNull(actual);
		assertEquals(1, actual.getPk().getParentId().getParentId1());
		assertEquals(2, actual.getPk().getParentId().getParentId2());
		assertEquals(3, actual.getPk().getDerivedId());
		assertEquals(1, actual.getParent().getPk().getParentId1());
		assertEquals(2, actual.getParent().getPk().getParentId2());
		assertEquals("parent", actual.getParent().getName());
		assertEquals("derived", actual.getName());
	}
}
