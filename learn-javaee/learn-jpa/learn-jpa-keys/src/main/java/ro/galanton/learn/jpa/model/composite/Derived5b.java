package ro.galanton.learn.jpa.model.composite;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

/**
 * An entity with a composite primary key (using @EmbeddedId) which is also a partial composite foreign key.
 */
@Entity
public class Derived5b {

	@EmbeddedId
	private Derived5bKey pk;
	
	@MapsId("parentId") // parentId = name of field in Derived5bKey
	@OneToOne
	private Parent5 parent;
	
	private String name;

	public Derived5b() {
	}

	public Derived5b(Derived5bKey pk, Parent5 parent, String name) {
		super();
		this.pk = pk;
		this.parent = parent;
		this.name = name;
	}

	public Derived5bKey getPk() {
		return pk;
	}

	public void setPk(Derived5bKey pk) {
		this.pk = pk;
	}

	public Parent5 getParent() {
		return parent;
	}

	public void setParent(Parent5 parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
