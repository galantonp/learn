package ro.galanton.learn.jpa.model.composite;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToOne;

/**
 * An entity with a composite primary key (using @IdClass) which is also a partial foreign key.
 */
@Entity
@IdClass(Derived4aKey.class)
public class Derived4a {
	
	@Id
	@OneToOne
	private Parent4 parent;
	
	@Id
	private long derivedId;
	
	private String name;

	public Derived4a() {
	}

	public Derived4a(Parent4 parent, long derivedId, String name) {
		this.parent = parent;
		this.derivedId = derivedId;
		this.name = name;
	}

	public Parent4 getParent() {
		return parent;
	}

	public void setParent(Parent4 parent) {
		this.parent = parent;
	}

	public long getDerivedId() {
		return derivedId;
	}

	public void setDerivedId(long derivedId) {
		this.derivedId = derivedId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
