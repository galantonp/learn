package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

public class Derived4aKey implements Serializable {

	private static final long serialVersionUID = 1L;

	private long parent; // must be same type as @Id in Parent4 and same name @Id attribute in Derived4a
	
	private long derivedId; // must be same type and name as @Id in Derived4a

	public Derived4aKey() {
	}

	public Derived4aKey(long parentId, long derivedId) {
		this.parent = parentId;
		this.derivedId = derivedId;
	}

	public long getId() {
		return parent;
	}
	
	public void setId(long id) {
		this.parent = id;
	}

	public long getDerivedId() {
		return derivedId;
	}

	public void setDerivedId(long derivedId) {
		this.derivedId = derivedId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (derivedId ^ (derivedId >>> 32));
		result = prime * result + (int) (parent ^ (parent >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Derived4aKey other = (Derived4aKey) obj;
		if (derivedId != other.derivedId)
			return false;
		if (parent != other.parent)
			return false;
		return true;
	}
	
}
