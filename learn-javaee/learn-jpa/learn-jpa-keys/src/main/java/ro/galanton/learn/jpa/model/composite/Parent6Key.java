package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Parent6Key implements Serializable {

	private static final long serialVersionUID = 1L;

	private long parentId1;
	
	private long parentId2;

	public Parent6Key() {
	}

	public Parent6Key(long parentId1, long parentId2) {
		super();
		this.parentId1 = parentId1;
		this.parentId2 = parentId2;
	}

	public long getParentId1() {
		return parentId1;
	}

	public void setParentId1(long parentId1) {
		this.parentId1 = parentId1;
	}

	public long getParentId2() {
		return parentId2;
	}

	public void setParentId2(long parentId2) {
		this.parentId2 = parentId2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (parentId1 ^ (parentId1 >>> 32));
		result = prime * result + (int) (parentId2 ^ (parentId2 >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parent6Key other = (Parent6Key) obj;
		if (parentId1 != other.parentId1)
			return false;
		if (parentId2 != other.parentId2)
			return false;
		return true;
	}
	
}
