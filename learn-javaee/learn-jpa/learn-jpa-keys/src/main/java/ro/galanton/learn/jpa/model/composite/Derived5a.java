package ro.galanton.learn.jpa.model.composite;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToOne;

/**
 * An entity with a composite key (using @IdClass) which is also a partial composite foreign key.
 */
@Entity
@IdClass(Derived5aKey.class)
public class Derived5a {
	
	@Id
	@OneToOne
	private Parent5 parent;
	
	@Id
	private long derivedId;
	
	private String name;

	public Derived5a() {
	}

	public Derived5a(Parent5 parent, long derivedId, String name) {
		super();
		this.parent = parent;
		this.derivedId = derivedId;
		this.name = name;
	}

	public Parent5 getParent() {
		return parent;
	}

	public void setParent(Parent5 parent) {
		this.parent = parent;
	}

	public long getDerivedId() {
		return derivedId;
	}

	public void setDerivedId(long derivedId) {
		this.derivedId = derivedId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
