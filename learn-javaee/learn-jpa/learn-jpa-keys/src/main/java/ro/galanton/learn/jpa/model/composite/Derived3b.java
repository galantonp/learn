package ro.galanton.learn.jpa.model.composite;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

@Entity
public class Derived3b {

	@EmbeddedId
	private Parent3Key pk;
	
	@MapsId
	@OneToOne
	private Parent3 parent;
	
	private String name;

	public Parent3Key getPk() {
		return pk;
	}

	public void setPk(Parent3Key pk) {
		this.pk = pk;
	}

	public Parent3 getParent() {
		return parent;
	}

	public void setParent(Parent3 parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
