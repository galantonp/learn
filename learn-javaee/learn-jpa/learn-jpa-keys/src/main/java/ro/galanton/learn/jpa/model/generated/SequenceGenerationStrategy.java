package ro.galanton.learn.jpa.model.generated;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class SequenceGenerationStrategy {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "demoGenerator")
	@SequenceGenerator(name = "demoGenerator", sequenceName = "demo_sequence")
	private int id;
	
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
