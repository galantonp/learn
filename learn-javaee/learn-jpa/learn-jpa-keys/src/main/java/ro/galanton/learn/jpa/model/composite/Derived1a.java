package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * An entity with a primary key that is also a foreign key.
 */
@Entity
public class Derived1a implements Serializable {

	// must implement Serializable (bug in Hibernate ?)
	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne
	private Parent1 parent;
	
	private String name;

	public Parent1 getParent() {
		return parent;
	}

	public void setParent(Parent1 parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
