package ro.galanton.learn.jpa.model.composite;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class Parent3 {

	@EmbeddedId
	private Parent3Key pk;
	
	private String name;

	public Parent3Key getPk() {
		return pk;
	}
	
	public void setPk(Parent3Key pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
