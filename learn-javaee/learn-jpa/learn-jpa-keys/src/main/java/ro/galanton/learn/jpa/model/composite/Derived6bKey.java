package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Derived6bKey implements Serializable {

	private static final long serialVersionUID = 1L;

	private Parent6Key parentId; // matches type of @Id in Parent6, can have any name
	
	private long derivedId; // matches type of @Id in Derived6b, can have any name

	public Derived6bKey() {
	}

	public Derived6bKey(Parent6Key parentId, long derivedId) {
		super();
		this.parentId = parentId;
		this.derivedId = derivedId;
	}

	public Parent6Key getParentId() {
		return parentId;
	}

	public void setParentId(Parent6Key parentId) {
		this.parentId = parentId;
	}

	public long getDerivedId() {
		return derivedId;
	}

	public void setDerivedId(long derivedId) {
		this.derivedId = derivedId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (derivedId ^ (derivedId >>> 32));
		result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Derived6bKey other = (Derived6bKey) obj;
		if (derivedId != other.derivedId)
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		return true;
	}
	
}
