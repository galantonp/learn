package ro.galanton.learn.jpa.model.composite;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(Parent5Key.class)
public class Parent5 {
	
	@Id
	private long parentId1;
	
	@Id
	private long parentId2;
	
	private String name;

	public Parent5() {
	}

	public Parent5(long id1, long id2, String name) {
		super();
		this.parentId1 = id1;
		this.parentId2 = id2;
		this.name = name;
	}

	public long getParentId1() {
		return parentId1;
	}

	public void setParentId1(long parentId1) {
		this.parentId1 = parentId1;
	}

	public long getParentId2() {
		return parentId2;
	}

	public void setParentId2(long parentId2) {
		this.parentId2 = parentId2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
