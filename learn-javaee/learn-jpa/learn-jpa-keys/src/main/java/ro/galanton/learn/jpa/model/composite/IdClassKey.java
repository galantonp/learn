package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

public class IdClassKey implements Serializable {

	// composite key classes must implement Serializable (2.4)
	private static final long serialVersionUID = 1L;

	private long id1;
	private long id2;

	// composite key classes must have no-arg constructor (2.4)
	public IdClassKey() {
	}
	
	public IdClassKey(long id1, long id2) {
		this.id1 = id1;
		this.id2 = id2;
	}
	
	// composite key classes must implement hashCode() (2.4)
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id1 ^ (id1 >>> 32));
		result = prime * result + (int) (id2 ^ (id2 >>> 32));
		return result;
	}
	
	// composite key classes must implement equals() (2.4)
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdClassKey other = (IdClassKey) obj;
		if (id1 != other.id1)
			return false;
		if (id2 != other.id2)
			return false;
		return true;
	}
	
}
