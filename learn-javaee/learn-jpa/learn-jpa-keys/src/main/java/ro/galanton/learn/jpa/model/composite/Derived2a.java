package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToOne;

/**
 * An entity with a composite primary key (using @IdClass) which is also a full foreign key.
 *
 */
@Entity
@IdClass(Parent2Key.class)
public class Derived2a implements Serializable {

	// must implement Serializable (bug in Hibernate ??)
	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne
	private Parent2 parent;
	
	private String name;

	public Parent2 getParent() {
		return parent;
	}
	
	public void setParent(Parent2 parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
