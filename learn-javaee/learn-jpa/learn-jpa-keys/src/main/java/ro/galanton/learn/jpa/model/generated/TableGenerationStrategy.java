package ro.galanton.learn.jpa.model.generated;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class TableGenerationStrategy {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "table_generator")
	@TableGenerator(
			name = "table_generator", 
			table = "sequences", 
			pkColumnName = "sequence_name",
			pkColumnValue = "TableGenerationStrategy",
			valueColumnName = "sequence_value")
	private int id;
	
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
