package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

public class Derived5aKey implements Serializable {

	private static final long serialVersionUID = 1L;

	private Parent5Key parent; // matches name of @Id in Derived5a and type of @Id in Parent5
	
	private long derivedId; // matches name and type of @Id in Derived5a

	public Derived5aKey() {
	}

	public Derived5aKey(Parent5Key parent, long derivedId) {
		super();
		this.parent = parent;
		this.derivedId = derivedId;
	}

	public Parent5Key getParent() {
		return parent;
	}

	public void setParent(Parent5Key parent) {
		this.parent = parent;
	}

	public long getDerivedId() {
		return derivedId;
	}

	public void setDerivedId(long derivedId) {
		this.derivedId = derivedId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (derivedId ^ (derivedId >>> 32));
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Derived5aKey other = (Derived5aKey) obj;
		if (derivedId != other.derivedId)
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		return true;
	}
	
}
