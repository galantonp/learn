package ro.galanton.learn.jpa.model.composite;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

/**
 * An entity with a composite primary key (using @EmbeddedId) which is also a partial foreign key.
 */
@Entity
public class Derived4b {

	@EmbeddedId
	private Derived4bKey pk;
	
	@MapsId("parentId") // parentId = name of corresponding field from Derived4bKey
	@OneToOne
	private Parent4 parent;
	
	private String name;

	public Derived4b() {
	}

	public Derived4b(Derived4bKey pk, Parent4 parent, String name) {
		this.pk = pk;
		this.parent = parent;
		this.name = name;
	}

	public Derived4bKey getPk() {
		return pk;
	}

	public void setPk(Derived4bKey pk) {
		this.pk = pk;
	}

	public Parent4 getParent() {
		return parent;
	}

	public void setParent(Parent4 parent) {
		this.parent = parent;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
