package ro.galanton.learn.jpa.model.composite;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Derived5bKey implements Serializable {

	private static final long serialVersionUID = 1L;

	private Parent5Key parentId; // matches type of @Id in Parent5, can have any name
	
	private long derivedId; // matches type of @Id in Derived5b, can have any name

	public Derived5bKey() {
	}

	public Derived5bKey(Parent5Key parentId, long derivedId) {
		super();
		this.parentId = parentId;
		this.derivedId = derivedId;
	}

	public Parent5Key getParentId() {
		return parentId;
	}

	public void setParentId(Parent5Key parentId) {
		this.parentId = parentId;
	}

	public long getDerivedId() {
		return derivedId;
	}

	public void setDerivedId(long derivedId) {
		this.derivedId = derivedId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (derivedId ^ (derivedId >>> 32));
		result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Derived5bKey other = (Derived5bKey) obj;
		if (derivedId != other.derivedId)
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		return true;
	}
	
}
