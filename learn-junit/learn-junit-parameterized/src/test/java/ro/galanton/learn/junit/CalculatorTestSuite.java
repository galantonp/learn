package ro.galanton.learn.junit;

import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ro.galanton.learn.junit.Calculator;

@RunWith(Suite.class)
@SuiteClasses({
	CalculatorTestSuite.InitialState.class,
	CalculatorTestSuite.ArithmeticOperation.class,
	CalculatorTestSuite.Memory.class
})
public class CalculatorTestSuite {

	public static abstract class Common {
		
		protected Calculator calculator;
		
		@Before
		public void setUp() {
			calculator = new Calculator();
		}
		
	}
	
	public static class InitialState extends Common {
		
		@Test
		public void is_zero() {
			assertThat(calculator.equals(), is(0d));
		}
		
		@Test
		public void can_be_set_with_enter() {
			assertThat(calculator.enter(5).equals(), is(5d));
		}
	}
	
	@RunWith(Suite.class)
	@SuiteClasses({
		ArithmeticOperation.Addition.class,
		ArithmeticOperation.Substraction.class,
		ArithmeticOperation.Multiplication.class,
		ArithmeticOperation.Division.class
	})
	public static class ArithmeticOperation {
	
		@RunWith(Parameterized.class)
		public static class Addition extends Common {
			
			@Parameters
			public static Collection<Object[]> parameters() {
				return Arrays.asList(
						new Object[] { 1d,  2d,  3d },
						new Object[] { 2d, -1d,  1d },
						new Object[] { 1d, -2d, -1d });
			}
			
			@Parameter(0)
			public double x;
			
			@Parameter(1)
			public double y;
			
			@Parameter(2)
			public double expected;
			
			@Test
			public void test() {
				// TODO name this in 'executable specs' style
				assertThat(calculator.enter(x).add(y).equals(), is(expected));
			}
			
		}
		
		@RunWith(Parameterized.class)
		public static class Substraction extends Common {
		
			@Parameters
			public static Collection<Object[]> parameters() {
				return Arrays.asList(
						new Object[] {  3d, 2d,  1d },
						new Object[] {  2d, 3d, -1d },
						new Object[] { -3d, 2d, -5d });
					// TODO what other parameters could be added ?
			}
			
			private double x;
			private double y;
			private double expected;
			
			public Substraction(double x, double y, double expected) {
				this.x = x;
				this.y = y;
				this.expected = expected;
			}
			
			
			@Test
			public void test() {
				assertThat(calculator.enter(x).substract(y).equals(), is(expected));
			}
			
		}
		
		public static class Multiplication extends Common {

			// TODO
			
			@Test
			public void not_implemented() {
			}
			
		}
		
		public static class Division extends Common {
			
			@Test(expected=IllegalArgumentException.class)
			public void by_zero_throws_exception() {
				calculator.enter(5).divide(0);
			}
			
			// TODO
		}
	}
	
	public static class Memory extends Common {
		
		// TODO
		
		@Test
		public void not_implemented() {
			
		}
		
	}
	
}
