package ro.galanton.learn.junit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ro.galanton.learn.junit.Calculator;

@RunWith(Suite.class)
@SuiteClasses({
	CalculatorTestSuite.InitialState.class,
	CalculatorTestSuite.ArithmeticOperation.class,
	CalculatorTestSuite.Memory.class
})
public class CalculatorTestSuite {

	public static abstract class Common {
		
		protected Calculator calculator;
		
		@Before
		public void setUp() {
			calculator = new Calculator();
		}
		
	}
	
	public static class InitialState extends Common {
		
		@Test
		public void is_zero() {
			assertThat(calculator.equals(), is(0d));
		}
		
		@Test
		public void can_be_set_with_enter() {
			assertThat(calculator.enter(5).equals(), is(5d));
		}
	}
	
	@RunWith(Suite.class)
	@SuiteClasses({
		ArithmeticOperation.Addition.class,
		ArithmeticOperation.Substraction.class,
		ArithmeticOperation.Multiplication.class,
		ArithmeticOperation.Division.class
	})
	public static class ArithmeticOperation {
	
		public static class Addition extends Common {
			
			@Test
			public void positive_plus_positive_is_positive() {
				assertThat(calculator.enter(1).add(2).equals(), is(3d));
			}
			
			@Test
			public void larger_positive_plus_smaller_negative_is_positive() {
				assertThat(calculator.enter(2).add(-1).equals(), is(1d));
			}
			
			@Test
			public void smaller_positive_plus_larger_negative_is_negative() {
				assertThat(calculator.enter(1).add(-2).equals(), is(-1d));
			}
			
		}
		
		public static class Substraction extends Common {
			
			@Test
			public void larger_positive_minus_smaller_positive_is_positive() {
				assertThat(calculator.enter(3).substract(2).equals(), is(1d));
			}
			
			@Test
			public void smaller_positive_minus_larger_positive_is_negative() {
				assertThat(calculator.enter(2).substract(3).equals(), is(-1d));
			}
			
			@Test
			public void negative_minus_positive_is_negative() {
				assertThat(calculator.enter(-3).substract(2).equals(), is(-5d));
			}
			
			// TODO 
			
		}
		
		public static class Multiplication extends Common {

			// TODO
			
			@Test
			public void not_implemented() {
			}
			
		}
		
		public static class Division extends Common {
			
			@Test(expected=IllegalArgumentException.class)
			public void by_zero_throws_exception() {
				calculator.enter(5).divide(0);
			}
			
			// TODO
		}
	}
	
	public static class Memory extends Common {
		
		// TODO
		
		@Test
		public void not_implemented() {
			
		}
		
	}
	
}
