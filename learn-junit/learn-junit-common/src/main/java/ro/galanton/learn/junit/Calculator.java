package ro.galanton.learn.junit;

public class Calculator {

	private double memory;
	private double value;
	
	public Calculator enter(double x) {
		this.value = x;
		return this;
	}
	
	public Calculator add(double x) {
		this.value += x;
		return this;
	}
	
	public Calculator substract(double x) {
		this.value -= x;
		return this;
	}
	
	public Calculator multiply(double x) {
		this.value *= x;
		return this;
	}
	
	public Calculator divide(double x) {
		if (x == 0d) {
			throw new IllegalArgumentException("division by zero");
		}
		
		this.value /= x;
		return this;
	}
	
	public double equals() {
		return value;
	}
	
	public Calculator memoryAdd() {
		memory += value;
		return this;
	}
	
	public double memoryRecall() {
		value = memory;
		return value;
	}
	
	public Calculator memoryClear() {
		memory = 0d;
		return this;
	}
	
}
