package ro.galanton.learn.junit.matchers;

import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;

public class IsPositiveDouble extends DiagnosingMatcher<Double> {

	@Override
	public void describeTo(Description description) {
		description.appendText("positive");
	}

	@Override
	protected boolean matches(Object item, Description mismatchDescription) {
		if (item == null) {
			mismatchDescription.appendText("null");
			return false;
		}
		
		if (!(item instanceof Double)) {
			mismatchDescription.appendValue(item).appendText(" is not a number").appendValue(item);
			return false;
		}
		
		Double number = (Double) item;
		
		if (number <= 0) {
			mismatchDescription.appendValue(number).appendText(" is not positive");
		}
		return number > 0;
	}

}
