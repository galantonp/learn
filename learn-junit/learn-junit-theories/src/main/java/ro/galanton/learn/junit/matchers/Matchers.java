package ro.galanton.learn.junit.matchers;

import org.hamcrest.Matcher;

public class Matchers {

	public static Matcher<Double> positiveDouble() {
		return new IsPositiveDouble();
	}
	
	public static Matcher<Double> negativeDouble() {
		return new IsNegativeDouble();
	}
}
