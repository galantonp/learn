package ro.galanton.learn.junit;

import static org.junit.Assume.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static ro.galanton.learn.junit.matchers.Matchers.*;
import static java.lang.Math.abs;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ro.galanton.learn.junit.Calculator;

@RunWith(Suite.class)
@SuiteClasses({
	CalculatorTestSuite.InitialState.class,
	CalculatorTestSuite.ArithmeticOperation.class,
	CalculatorTestSuite.Memory.class
})
public class CalculatorTestSuite {

	public static abstract class Common {
		
		protected Calculator calculator;
		
		@Before
		public void setUp() {
			calculator = new Calculator();
		}
		
	}
	
	public static class InitialState extends Common {
		
		@Test
		public void is_zero() {
			assertThat(calculator.equals(), is(0d));
		}
		
		@Test
		public void can_be_set_with_enter() {
			assertThat(calculator.enter(5).equals(), is(5d));
		}
	}
	
	@RunWith(Suite.class)
	@SuiteClasses({
		ArithmeticOperation.Addition.class,
		ArithmeticOperation.Substraction.class,
		ArithmeticOperation.Multiplication.class,
		ArithmeticOperation.Division.class
	})
	public static class ArithmeticOperation {
	
		@RunWith(Theories.class)
		public static class Addition extends Common {

			@DataPoint
			public static double largePositive = 5;
			
			@DataPoint
			public static double smallPositive = 3;
			
			@DataPoint
			public static double zero = 0;
			
			@DataPoint
			public static double smallNegative = -3;
			
			@DataPoint
			public static double largeNegative = -5;
			
			@Theory
			public void positive_plus_positive_is_positive(double x, double y) {
				System.out.println("### x = " + x + ", y = " + y);
				assumeThat(x > 0, is(true));
				assumeThat(y > 0, is(true));
				System.out.println("   ### assumptions passed");
				
				assertThat(calculator.enter(x).add(y).equals(), is(positiveDouble()));
			}
			
			@Theory
			public void large_positive_plus_small_negative_is_positive(double x, double y) {
				assumeThat(x > 0, is(true));
				assumeThat(y < 0, is(true));
				assumeThat(abs(x) > abs(y), is(true));
				
				assertThat(calculator.enter(x).add(y).equals(), is(positiveDouble()));
			}
			
		}
		
		@RunWith(Theories.class)
		public static class Substraction extends Common {
			
			@DataPoints
			public static double[] values = { 5d, 3d, 0d, -3d, -5d };
			
			@Theory
			public void larger_positive_minus_smaller_positive_is_positive(double x, double y) {
				assumeThat(x, is(positiveDouble()));
				assumeThat(y, is(positiveDouble()));
				assumeThat(x, is(greaterThan(y)));
				
				assertThat(calculator.enter(x).substract(y).equals(), is(positiveDouble()));
			}
			
			@Theory
			public void smaller_positive_minus_larger_positive_is_negative(double x, double y) {
				assumeThat(x, is(positiveDouble()));
				assumeThat(y, is(positiveDouble()));
				assumeThat(x, is(lessThan(y)));
				
				assertThat(calculator.enter(x).substract(y).equals(), is(negativeDouble()));
			}
			
			@Test
			public void negative_minus_positive_is_negative() {
				// TODO
			}
			
			// TODO 
			
		}
		
		@RunWith(Theories.class)
		public static class Multiplication extends Common {

			@DataPoints("positive")
			public static double[] positive = { 1d, 2d, 3d };
			
			@DataPoints("negative")
			public static double[] negative = { -1d, -2d, -3d };
			
			@Theory
			public void positive_times_positive_is_positive(
					@FromDataPoints("positive") double x, 
					@FromDataPoints("positive") double y) {
				
				assertThat(calculator.enter(x).multiply(y).equals(), is(positiveDouble()));
			}
			
			@Theory
			public void negative_times_negative_is_positive(
					@FromDataPoints("negative") double x,
					@FromDataPoints("negative") double y) {
				
				assertThat(calculator.enter(x).multiply(y).equals(), is(positiveDouble()));
			}
		}
		
		// TODO use parameter supplier
		public static class Division extends Common {
			
			@Test(expected=IllegalArgumentException.class)
			public void by_zero_throws_exception() {
				calculator.enter(5).divide(0);
			}
			
			// TODO
		}
	}
	
	public static class Memory extends Common {
		
		// TODO
		
		@Test
		public void not_implemented() {
			
		}
		
	}
	
}
