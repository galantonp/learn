#ifndef ULT_H
#define ULT_H

#include <ucontext.h>

// log nothing
#define ULT_NONE 0

// log creation of resources
#define ULT_CREATE 1

// log when a thread locks a mutex
#define ULT_LOCK 2

// log when a thread unlocks a mutex
#define ULT_UNLOCK 4

// log scheduler messages
#define ULT_SCHED 8

// log when a thread is suspended (locking or joining)
#define ULT_SUSPEND 16

// log when a thread is resumed (no longer locking or joining)
#define ULT_RESUME 32

// log everything
#define ULT_ALL 63

typedef int ult_t;
typedef int ult_mutex_t;

// initializes the ULT library
void ult_init();

// creates a thread that executes the specified function
ult_t ult_create(void (*f)(void));

// joins the thread with the specified id (waits until it finishes)
void ult_join(ult_t thread);

// returns the id of the current thread
ult_t ult_self();

// creates a new mutex that is initially not locked
ult_mutex_t ult_mutex_create();

// obtains a lock on the specified mutex
void ult_mutex_lock(ult_mutex_t mutex);

// releases the lock on the specified mutex
void ult_mutex_unlock(ult_mutex_t mutex);

// destroys the specified mutex
void ult_mutex_destroy(ult_mutex_t mutex);

// control logging level
// combine ULT_CREATE, ULT_LOCK, ULT_UNLOCK, ULT_SCHED, etc. with bitwise OR
void ult_log(int flags);

#endif
