package ro.galanton.learn.spring.boot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import ro.galanton.learn.spring.boot.config.RootConfig;

public class Application {

	private static final List<Application> runningApplications = new ArrayList<Application>();
	
	private ApplicationContext context;
	
	public Application run(String... args) {
		context = SpringApplication.run(RootConfig.class, args);
		return this;
	}
	
	public ApplicationContext getApplicationContext() {
		return context;
	}
	
	public static void main(String... args) {
//		new SpringApplicationBuilder()
//			.parent(RootConfig.class)
//			.child(ServerConfig.class)
//			.run(args);
		
		
		runningApplications.add(new Application().run(args));
	}
	
	public static List<Application> getRunningApplications() {
		return Collections.unmodifiableList(runningApplications);
	}
}
