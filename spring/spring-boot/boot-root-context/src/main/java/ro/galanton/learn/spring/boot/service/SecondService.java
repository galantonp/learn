package ro.galanton.learn.spring.boot.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import ro.galanton.learn.spring.boot.annotation.RootService;
import ro.galanton.learn.spring.boot.repository.SecondRepository;

@RootService
public class SecondService implements ApplicationContextAware {
	
	private SecondRepository secondRepository;
	
	@Autowired
	public SecondService(SecondRepository secondRepository) {
		System.out.println("### second service created");
		this.secondRepository = secondRepository;
	}
	
	public void test() {
		System.out.println("### second service");
		secondRepository.test();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("### second service context: " + applicationContext);
	}

	
}
