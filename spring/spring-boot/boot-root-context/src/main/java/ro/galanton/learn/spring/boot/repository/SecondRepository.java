package ro.galanton.learn.spring.boot.repository;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import ro.galanton.learn.spring.boot.annotation.RootRepository;

@RootRepository
public class SecondRepository implements ApplicationContextAware {

	public SecondRepository() {
		System.out.println("### second repository created");
	}
	
	public void test() {
		System.out.println("### second repository");
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("### second repository context: " + applicationContext);
	}

}
