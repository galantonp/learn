package ro.galanton.learn.spring.boot.repository;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Repository;

import ro.galanton.learn.spring.boot.annotation.RootContext;

@RootContext
@Repository
public class FirstRepository implements ApplicationContextAware {

	public FirstRepository() {
		System.out.println("### first repository created");
	}
	
	public void test() {
		System.out.println("### first repository");
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("### first repository context: " + applicationContext);
	}

}
