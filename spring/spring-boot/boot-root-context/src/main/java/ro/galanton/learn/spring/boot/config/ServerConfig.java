package ro.galanton.learn.spring.boot.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import ro.galanton.learn.spring.boot.Application;
import ro.galanton.learn.spring.boot.annotation.ServerContext;

@Configuration
@EnableWebMvc
@ServerContext
@ComponentScan(basePackageClasses = Application.class, includeFilters = @Filter(ServerContext.class), useDefaultFilters = false)
public class ServerConfig {

	@Autowired
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("### server config created in context: " + applicationContext);
	}
}
