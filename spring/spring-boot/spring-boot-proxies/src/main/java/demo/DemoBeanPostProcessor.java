package demo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ClassUtils;

public class DemoBeanPostProcessor implements BeanPostProcessor {

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if ("HibernateRepository".equals(beanName)) {
			System.out.println("#");
		}
		if (hasAnnotation(bean)) {
			return createProxy(bean);
		}
		
		return bean;
	}
	
	private boolean hasAnnotation(Object bean) {
		UsesSQL annotation = AnnotationUtils.findAnnotation(bean.getClass(), UsesSQL.class);
		return annotation != null;
	}
	
	private Object createProxy(Object bean) {
		InvocationHandler handler = new DemoInvocationHandler(bean);
		Class<?>[] interfaces = ClassUtils.getAllInterfaces(bean);
		Object proxy = Proxy.newProxyInstance(this.getClass().getClassLoader(), interfaces, handler);

		return proxy;
	}

}
