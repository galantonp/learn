package demo;

import org.springframework.stereotype.Repository;

@UsesSQL
@Repository("HibernateRepository")
public class HibernateRepository implements IRepository {

	public HibernateRepository() {
		System.out.println("HibernateRepository");
	}
	
	@Override
	public void query() {
		System.out.println("HibernateRepository.query");
	}

}
