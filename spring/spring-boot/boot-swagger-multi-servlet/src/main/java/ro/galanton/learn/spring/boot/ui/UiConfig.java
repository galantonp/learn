package ro.galanton.learn.spring.boot.ui;

import javax.servlet.ServletContext;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import ro.galanton.learn.spring.boot.api.ApiConfig;

@SpringBootApplication
public class UiConfig {

	@Bean
	public ServletRegistrationBean apiServlet(WebApplicationContext parentContext, ServletContext servletContext) {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.setServletContext(servletContext);
		context.setParent(parentContext);
		context.register(ApiConfig.class);
		context.refresh();
		
		ServletRegistrationBean servlet = new ServletRegistrationBean(new DispatcherServlet(context), "/api/v1/*");
		servlet.setName("apiServlet");
		servlet.setLoadOnStartup(1);
		
		return servlet;
	}
}
