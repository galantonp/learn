package ro.galanton.learn.spring.boot.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.paths.SwaggerPathProvider;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;

@Configuration
@ComponentScan
@EnableWebMvc
@EnableSwagger
public class ApiConfig extends WebMvcConfigurerAdapter {

	@Bean
	public SwaggerSpringMvcPlugin swaggerPlugin(SpringSwaggerConfig swaggerConfig) {
		return new SwaggerSpringMvcPlugin(swaggerConfig)
				.pathProvider(new CustomPathProvider())
				.build();
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// the resources for the /api servlet need to be placed in a classpath directory which not 
		// picked up by default by the other servlet (eg. classpath:/static/).
		registry.addResourceHandler("/docs/**").addResourceLocations("classpath:/swagger-ui/");
	}
	
	
	public static class CustomPathProvider extends SwaggerPathProvider {
		
		@Override
		protected String applicationPath() {
			return "/api/v1";
		}
		
		@Override
		protected String getDocumentationPath() {
			return "/";
		}
		
	}
}
