package ro.galanton.learn.spring.boot.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

	public ApiController() {
		System.out.println("### api controller created");
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public void test() {
		System.out.println("### api");
	}
}
