package ro.galanton.learn.spring.boot.ui;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UiController {

	public UiController() {
		System.out.println("### ui controller created");
	}

	@RequestMapping(value = "/ui", method = RequestMethod.GET)
	public void test() {
		System.out.println("### ui");
	}
}
