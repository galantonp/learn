package ro.galanton.learn.spring.boot;

import org.springframework.boot.SpringApplication;

import ro.galanton.learn.spring.boot.ui.UiConfig;

public class Application {

	public static void main(String[] args) {
		SpringApplication.run(UiConfig.class, args);
	}
}
