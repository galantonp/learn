package test.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.annotation.RequireRole;

@RestController
public class TestController {

	@RequestMapping("/first")
	@RequireRole("ROLE_ADMIN")
	public void first() {
		System.out.println("### first");
	}
	
	@RequestMapping("/second")
	@RequireRole("ROLE_USER")
	public void second() {
		System.out.println("### second");
	}
	
}
