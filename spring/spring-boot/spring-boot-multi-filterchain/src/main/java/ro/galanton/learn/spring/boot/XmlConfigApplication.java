package ro.galanton.learn.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan("ro.galanton.learn.spring.boot.controller")
@Import(EmbeddedServletContainerAutoConfiguration.class)
@EnableWebMvc
public class XmlConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmlConfigApplication.class, args);
	}
}
