package ro.galanton.learn.spring.webmvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BasicController {

	public BasicController() {
		System.out.println("### BasicController created");
	}
	
	@RequestMapping("/basic")
	public String index() {
		System.out.println("### BasicController.index called");
		return "basic-view";
	}
	
}
