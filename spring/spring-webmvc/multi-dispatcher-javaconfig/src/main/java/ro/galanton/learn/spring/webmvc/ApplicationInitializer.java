package ro.galanton.learn.spring.webmvc;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import ro.galanton.learn.spring.webmvc.child.ChildController;
import ro.galanton.learn.spring.webmvc.main.MainController;

public class ApplicationInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext ctx) throws ServletException {
		System.out.println("### initializing web application");
		
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.setServletContext(ctx);
		rootContext.scan(MainController.class.getPackage().getName());
		rootContext.refresh();
		
		AnnotationConfigWebApplicationContext childContext = new AnnotationConfigWebApplicationContext();
		childContext.setParent(rootContext);
		childContext.setServletContext(ctx);
		childContext.scan(ChildController.class.getPackage().getName());
		childContext.refresh();
		
		ServletRegistration.Dynamic main = ctx.addServlet("main", new DispatcherServlet(rootContext));
		main.addMapping("/*");
		main.setLoadOnStartup(1);
		
		ServletRegistration.Dynamic child = ctx.addServlet("child", new DispatcherServlet(childContext));
		child.addMapping("/child/*");
		child.setLoadOnStartup(1);
	}

}
