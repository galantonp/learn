package ro.galanton.learn.spring.webmvc.child;

import org.springframework.stereotype.Service;

@Service
public class ChildService {

	public ChildService() {
		System.out.println("### child service created");
	}
	
	public void test() {
		System.out.println("### child service");
	}
	
}
