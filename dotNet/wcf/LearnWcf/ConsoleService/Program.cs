﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace ConsoleService
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting service: Calculator");

            ServiceHost host = null;
            try
            {
                host = new ServiceHost(typeof(CalculatorService), new Uri("http://localhost:8095/wcf/calculator"));

                // enabled metadata exchange so we can see the WSDL
                host.Description.Behaviors.Add(new ServiceMetadataBehavior() { HttpGetEnabled = true });

                // start the service host
                host.Open();

                Console.WriteLine("Service started, press any key to exit ...");
                Console.ReadKey(true);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                if (host != null)
                {
                    Console.WriteLine("Closing service: Calculator");
                    host.Close();
                }
            }
        }
    }
}
