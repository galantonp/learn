﻿using System;
namespace ConsoleService
{
    class CalculatorService : ICalculatorService
    {
        public CalculatorService()
        {
            Console.WriteLine("New CalculatorService instance created");
        }

        public int add(int x, int y)
        {
            Console.WriteLine("[CalculatorService] [add]: x={0}, y={1}", x, y);

            return x + y;
        }
    }
}
