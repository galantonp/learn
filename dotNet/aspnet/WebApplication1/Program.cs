using Microsoft.EntityFrameworkCore;
using Serilog;
using WebApplication1;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddApplicationInsightsTelemetry();
builder.Services.AddControllers();
builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("postgres"))
);
builder.Services.AddDbContext<AppDbContext2>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("postgres2"))
);

builder.Host.UseSerilog((context, configuration) => configuration.ReadFrom.Configuration(context.Configuration));

var app = builder.Build();
//app.MapControllers();
// 1. request
// 2. logging middleware
// 3. authentication middleware -> ldap
// 4. authorization middleware -> db
// 5. downstream middleware -> HttpClient
//app.Use((context, next) =>
//{
//    next.Invoke(context);
//    context.Response.StatusCode = 405;
//});


app.Run();
