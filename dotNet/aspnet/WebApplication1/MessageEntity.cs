﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1
{

    [Table("messages")]
    public class MessageEntity
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("message")]
        public string? Message { get; set; }
    }
}
