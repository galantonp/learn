﻿Simple POC for:
- sync CRUD API
- simple entity with no relationships
- Postgres DB
- log with Serilog
- send logs and telemetry to Application Insights