﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<TestController> _logger;

        public TestController(AppDbContext context, ILogger<TestController> logger)
        {
            _context = context;
            _logger = logger;

            _logger.LogDebug("TestController created");
        }

        [HttpGet]
        public ActionResult<MessageEntity[]> GetAll()
        {
            _logger.LogDebug("TestController GetAll");
            return Ok(_context.Messages.ToArray());
        }

        [HttpGet("{id}")]
        public MessageEntity? GetOne(long id)
        {
            _logger.LogDebug("TestController GetOne: {}", id);

            // this uses LINQ
            // SELECT id, message FROM messages WHERE id = ...
            return _context.Messages.Where(m => m.Id == id).SingleOrDefault();

            // this generates a subquery: SELECT w.id, w.messages FROM (SELECT * FROM messages WHERE id = @p0) AS w LIMIT 2
            //return _context.Messages.FromSqlInterpolated($"SELECT * FROM messages WHERE id = {id}").SingleOrDefault();

            // this also generates a subquery, but without using parameters
            // SELECT w.id, w.messages FROM (SELECT * FROM messages WHERE id = 1) AS w LIMIT 2
            //return _context.Messages.FromSqlRaw($"SELECT * FROM messages WHERE id = {id}").SingleOrDefault();
        }

        [HttpPost]
        public MessageEntity Add(MessageEntity message)
        {
            _logger.LogDebug("TestController Add: {}", message);
            _context.Messages.Add(message);
            _context.SaveChanges();

            return message;
        }

        [HttpPut("{id}")]
        public MessageEntity Replace(long id, MessageEntity newMessage)
        {
            _logger.LogDebug("TestController Replace: {}, {}", id, newMessage);
            var message = _context.Messages.Where(m => m.Id == id).Single();
            message.Message = newMessage.Message;
            _context.SaveChanges();

            return message;
        }

        [HttpDelete("{id}")]
        public void Delete(long id)
        {
            _logger.LogDebug("TestController Delete: {}", id);
            var message = _context.Messages.Where(m => m.Id == id).Single();
            _context.Messages.Remove(message);
            _context.SaveChanges();
        }
    }
}
