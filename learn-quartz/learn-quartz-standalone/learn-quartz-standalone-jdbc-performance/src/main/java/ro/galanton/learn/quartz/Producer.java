package ro.galanton.learn.quartz;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import ro.galanton.learn.quartz.job.ProducerJob;

public class Producer {

	public static void main(String[] args) throws Exception {
		SchedulerFactory consumerFactory = new StdSchedulerFactory("quartz-consumer.properties");
		Scheduler consumerScheduler = consumerFactory.getScheduler();
		consumerScheduler.standby();
		consumerScheduler.clear();
		
		SchedulerFactory producerFactory = new StdSchedulerFactory("quartz-producer.properties");
		Scheduler producerScheduler = producerFactory.getScheduler();
		producerScheduler.getContext().put("consumerScheduler", consumerScheduler);
		producerScheduler.start();
		
		producerScheduler.clear();

		JobDetail jobDetail = JobBuilder.newJob(ProducerJob.class).withIdentity("producer-job").build();
		SimpleTrigger trigger = TriggerBuilder.newTrigger()
			.withIdentity("producer-trigger")
			.startNow()
			.withSchedule(SimpleScheduleBuilder.simpleSchedule()
					.withIntervalInMilliseconds(50) // 20 jobs per second
					.repeatForever())
			.build();
		
		producerScheduler.scheduleJob(jobDetail, trigger);
	}
}
