package ro.galanton.learn.quartz.listener;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingJobListener implements JobListener {

	private static final Logger LOG = LoggerFactory.getLogger(LoggingJobListener.class);

	@Override
	public String getName() {
		return "logging-job-listener";
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		LOG.info("starting job: {}", context.getJobDetail().getKey());
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		LOG.info("job {} execution vetoed", context.getJobDetail().getKey());
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		LOG.info("finished job: {}", context.getJobDetail().getKey());
	}
}
