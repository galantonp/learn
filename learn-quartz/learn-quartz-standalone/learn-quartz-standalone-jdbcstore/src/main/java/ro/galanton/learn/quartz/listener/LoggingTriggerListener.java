package ro.galanton.learn.quartz.listener;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.quartz.TriggerListener;

public class LoggingTriggerListener implements TriggerListener {

	private static final Logger LOG = LoggerFactory.getLogger(LoggingTriggerListener.class);
	
	@Override
	public String getName() {
		return "logging-trigger-listener";
	}

	@Override
	public void triggerFired(Trigger trigger, JobExecutionContext context) {
		LOG.info("trigger fired: {}", trigger.getKey());
	}

	@Override
	public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
		return false;
	}

	@Override
	public void triggerMisfired(Trigger trigger) {
		LOG.info("trigger misfired: {}", trigger.getKey());
	}

	@Override
	public void triggerComplete(Trigger trigger, JobExecutionContext context, CompletedExecutionInstruction triggerInstructionCode) {
		LOG.info("trigger complete: {}", trigger.getKey());
	}

}
