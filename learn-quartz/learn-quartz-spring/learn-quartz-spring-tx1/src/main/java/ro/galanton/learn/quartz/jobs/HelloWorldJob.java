package ro.galanton.learn.quartz.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HelloWorldJob implements Job {

	public HelloWorldJob() {
		System.out.println("HelloWorldJob instantiated.");
	}

	// TODO make @Transactional annotation work
	@Override
	@Transactional
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("Hello World!");
	}
}
