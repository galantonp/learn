package ro.galanton.learn.quartz.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MessageJob implements Job {

	private String message;
	
	public MessageJob() {
		System.out.println("HelloWorldJob instantiated.");
	}
	
	public void setMessage(String message) {
		// this property is set courtesy of SpringBeanJobFactory
		this.message = message;
	}

	// TODO make @Transactional annotation work
	@Override
	@Transactional
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println(message);
	}
}
