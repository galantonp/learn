package ro.galanton.learn.quartz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ro.galanton.learn.quartz.jobs.HelloWorldJob;
import ro.galanton.learn.quartz.jobs.MessageJob;

@SpringBootApplication
@EnableTransactionManagement
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public SchedulerFactoryBean scheduler(JobFactory jobFactory, DataSource dataSource,
			PlatformTransactionManager txManager, List<Trigger> triggers) {
		
		SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();

		// TODO configure scheduler
		schedulerFactory.setJobFactory(jobFactory);
		schedulerFactory.setDataSource(dataSource);
		schedulerFactory.setTransactionManager(txManager);
		
		// schedule a triggers (this could be done anywhere, not just on bean creation)
		schedulerFactory.setTriggers(triggers.toArray(new Trigger[triggers.size()]));
		return schedulerFactory;
	}

	@Bean
	public SpringBeanJobFactory jobFactory() {
		SpringBeanJobFactory jobFactory = new SpringBeanJobFactory();

		// TODO configure job factory ?

		return jobFactory;
	}

	@Bean
	public JobDetailFactoryBean helloWorldJobDetail() {
		JobDetailFactoryBean jobDetail = new JobDetailFactoryBean();
		jobDetail.setJobClass(HelloWorldJob.class);
		jobDetail.setDurability(true);

		return jobDetail;
	}
	
	@Bean
	public SimpleTriggerFactoryBean helloWorldJobTrigger(@Qualifier("helloWorldJobDetail") JobDetail jobDetail) {
		SimpleTriggerFactoryBean jobTrigger = new SimpleTriggerFactoryBean();

		jobTrigger.setJobDetail(jobDetail);
		jobTrigger.setRepeatInterval(2000); // millis
		jobTrigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);

		return jobTrigger;
	}
	
	@Bean
	public JobDetailFactoryBean messageJobDetail() {
		JobDetailFactoryBean jobDetail = new JobDetailFactoryBean();
		jobDetail.setJobClass(MessageJob.class);
		jobDetail.setDurability(true);

		Map<String, String> jobData = new HashMap<>();
		jobData.put("message", "Custom message!");
		jobDetail.setJobDataAsMap(jobData);
		
		return jobDetail;
	}
	
	@Bean
	public SimpleTriggerFactoryBean messageJobTrigger(@Qualifier("messageJobDetail") JobDetail jobDetail) {
		SimpleTriggerFactoryBean jobTrigger = new SimpleTriggerFactoryBean();

		jobTrigger.setJobDetail(jobDetail);
		jobTrigger.setRepeatInterval(3000); // millis
		jobTrigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);

		return jobTrigger;
	}

	@Bean
	public DataSource dataSource() {
		// TODO replace with non-embedded database and use DataSourceInitializer to init/clean it up
		return new EmbeddedDatabaseBuilder()
				.setType(EmbeddedDatabaseType.H2)
				.addScript("tables_h2.sql")
				.build();
	}

}
