package ro.galanton.learn.quartz.factory;

import org.quartz.Job;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Creates job instances assuming that there is a prototype-scoped bean defined in the Spring 
 * Application Context for every job class defined in JobDetail instances.
 * 
 * <p>
 * Job instances created with this factory support annotation-driven transactions, autowiring and 
 * all other regular Spring Beans features. 
 * </p>
 * @author Petru Galanton
 */
public class CustomJobFactory implements JobFactory, ApplicationContextAware {

	// TODO this class could extend AdaptableJobFactory, so that the jobs don't need to 
	// implement Quartz' Job interface, but may implement Java's Runnable interface instead
	
	private ApplicationContext springContext;
	
	@Override
	public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws SchedulerException {
		Class<? extends Job> jobClass = bundle.getJobDetail().getJobClass();
		return springContext.getBean(jobClass);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.springContext = applicationContext;
	}

}
