package ro.galanton.learn.quartz.jobs.impl;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ro.galanton.learn.quartz.jobs.HelloWorldJob;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HelloWorldJobImpl implements Job, HelloWorldJob {

	public HelloWorldJobImpl() {
		System.out.println("HelloWorldJob instantiated.");
	}

	@Override
	@Transactional
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO set a breakpoint here and inspect the stack to ensure this method is transactional
		System.out.println("Hello World!");
	}
}
