package ro.galanton.learn.quartz.service;

import org.springframework.stereotype.Service;

@Service
public class DemoService {

	public void demo() {
		System.out.println("Autowiring works!");
	}
	
}
